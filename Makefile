update_all_in_one_yaml: update_helmchart
	helm template esrc deploy/helm/es-rollover-controller/ > deploy/default-all-in-one.yaml

update_helmchart:
	./hack/update-helmchart.sh

start_elastic:
	docker run -d --name elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.8.0

start_controller:
	go build . && ./es-rollover-controller --expose_prometheus_metrics=true --elasticsearch_host=http://localhost --kubeconfig="${HOME}/.kube/config"

lint_helm:
	helm template esrc deploy/helm/es-rollover-controller/ |kubeval --
	helm lint deploy/helm/es-rollover-controller/

