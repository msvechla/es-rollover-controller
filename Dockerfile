FROM gcr.io/distroless/static-debian11

COPY es-rollover-controller /
CMD ["/es-rollover-controller"]
