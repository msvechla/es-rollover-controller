#!/bin/bash

echo "setting up elasticsearch in docker for testing..."
docker run -d --name elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.3.2 

echo "building and running controller locally against elasticsearch in docker and minikube..."
go build . && ./es-rollover-controller --kubeconfig /Users/marius.svechla/.kube/config --elasticsearch_host=http://localhost --elasticsearch_port=9200 --ilm_policy_overwrite=true