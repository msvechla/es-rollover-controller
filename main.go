/*
Copyright 2017 The Kubernetes Authors.
Modifications Copyright 2019 Marius Svechla

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"

	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"

	"k8s.io/sample-controller/pkg/signals"
)

func main() {
	config := config.InitApp(false)

	// set up signals so we handle the first shutdown signal gracefully
	stopCh := signals.SetupSignalHandler()

	cfg, err := clientcmd.BuildConfigFromFlags(config.FlagMasterURL, config.FlagKubeconfig)
	if err != nil {
		log.Fatalf("Error building kubeconfig: %s", err.Error())
	}

	kubeClient, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		log.Fatalf("Error building kubernetes clientset: %s", err.Error())
	}

	podInformerFactory := createRolloverIndexPodInformerFactory(
		kubeClient,
		config.FlagRolloverIndexLabelKey,
		config.FlagRolloverIndexAnnotationKey,
		config.FlagNamespaceScope,
	)

	controller := NewController(
		kubeClient,
		podInformerFactory.Core().V1().Pods(),
		config,
		stopCh.Done(),
	)

	podInformerFactory.Start(stopCh.Done())

	if err = controller.Run(config.FlagWorkers, stopCh.Done()); err != nil {
		log.Fatalf("Error running controller: %s", err.Error())
	}
}
