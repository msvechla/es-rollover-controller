{
  "branches": [
    "main",
    "master"
  ],
  "tagFormat": "${version}",
  "plugins": [
    [
      "@semantic-release/commit-analyzer",
      {
        "preset": "conventionalcommits",
        "releaseRules": [
          {
            "type": "docs",
            "release": false
          },
          {
            "type": "refactor",
            "release": "patch"
          },
          {
            "type": "test",
            "release": false
          },
          {
            "type": "chore",
            "release": false
          }
        ],
        "parserOpts": {
          "noteKeywords": [
            "BREAKING CHANGE",
            "BREAKING CHANGES",
            "BREAKING"
          ]
        }
      },
    ],
    [
       "@google/semantic-release-replace-plugin",
      {
        "replacements": [
          {
            "files": ["internal/pkg/config/config.go"],
            "from": "AppVersion.*= \"v.*\"",
            "to": "AppVersion = \"v${nextRelease.version}\"",
            "results": [
              {
                "file": "internal/pkg/config/config.go",
                "hasChanged": true,
                "numMatches": 1,
                "numReplacements": 1
              }
            ],
            "countMatches": true
          },
          {
            "files": ["deploy/helm/es-rollover-controller/values.yaml"],
            "from": "tag:\\s+\".*\"",
            "to": "tag: \"${nextRelease.version}\"",
            "results": [
              {
                "file": "deploy/helm/es-rollover-controller/values.yaml",
                "hasChanged": true,
                "numMatches": 1,
                "numReplacements": 1
              }
            ],
            "countMatches": true
          },
          {
            "files": ["deploy/helm/es-rollover-controller/Chart.yaml"],
            "from": "appVersion:\\s+v.*",
            "to": "appVersion: v${nextRelease.version}",
            "results": [
              {
                "file": "deploy/helm/es-rollover-controller/Chart.yaml",
                "hasChanged": true,
                "numMatches": 1,
                "numReplacements": 1
              }
            ],
            "countMatches": true
          },
          {
            "files": ["deploy/helm/es-rollover-controller/Chart.yaml"],
            "from": "version:\\s+.*",
            "to": "version: ${nextRelease.version}",
            "results": [
              {
                "file": "deploy/helm/es-rollover-controller/Chart.yaml",
                "hasChanged": true,
                "numMatches": 1,
                "numReplacements": 1
              }
            ],
            "countMatches": true
          }
        ]
      }
    ],
    [
       "@semantic-release/exec", {
         "prepareCmd": "make update_all_in_one_yaml"
       }
    ],
    [
      "@semantic-release/release-notes-generator",
      {
        "preset": "conventionalcommits",
        "parserOpts": {
          "noteKeywords": [
            "BREAKING CHANGE",
            "BREAKING CHANGES",
            "BREAKING"
          ]
        },
        "presetConfig": {
          "types": [
            {
              "type": "fix",
              "section": "Bug Fixes",
              "hidden": false
            },
            {
              "type": "feat",
              "section": "Features",
              "hidden": false
            },
            {
              "type": "refactor",
              "section": "Refactor",
              "hidden": false
            },
            {
              "type": "test",
              "hidden": true
            },
            {
              "type": "chore",
              "hidden": true
            },
            {
              "type": "docs",
              "hidden": true
            }
          ]
        }
      }
    ],
    [
      "@semantic-release/changelog",
      {
        "changelogFile": "CHANGELOG.md"
      }
    ],
    [
      "@semantic-release/gitlab",
      {
        "assets": [
          {
            "path": "CHANGELOG.md"
          }
        ]
      }
    ],
    [
      "@semantic-release/git",
      {
        "message": "chore(release): ${nextRelease.version} \n\n${nextRelease.notes}",
        "assets": [
          "CHANGELOG.md",
          "deploy/**/*",
          "internal/pkg/config/config.go"
        ]
      }
    ]
  ]
}

