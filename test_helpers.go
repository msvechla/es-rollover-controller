package main

import (
	"context"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"

	docker "docker.io/go-docker"
	"docker.io/go-docker/api/types"
	"docker.io/go-docker/api/types/container"
	"docker.io/go-docker/api/types/filters"
	"docker.io/go-docker/api/types/network"
	"github.com/docker/go-connections/nat"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/stretchr/testify/assert"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeinformers "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/tools/record"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/instrumentation"
	"mariussvechla.com/es-rollover-controller/internal/pkg/kbutil"
)

var esVersions = []string{
	"8.6.2",
}

type fixture struct {
	t            *testing.T
	dockerClient *docker.Client

	kubeclient *k8sfake.Clientset
	// Objects to put in the store.
	podLister []*corev1.Pod
}

func newFixture(t *testing.T) *fixture {
	f := &fixture{}
	f.t = t

	cli, err := docker.NewEnvClient()
	if err != nil {
		panic(err)
	}

	f.dockerClient = cli
	return f
}

// newPod creates a pod with the specified labels and annotations
func newPod(name string, labels, annotations map[string]string) *corev1.Pod {
	return &corev1.Pod{
		TypeMeta: metav1.TypeMeta{APIVersion: corev1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   metav1.NamespaceDefault,
			Labels:      labels,
			Annotations: annotations,
		},
		Spec: corev1.PodSpec{},
	}
}

// deletePodByName deletes a pod from a podlist by name
func deletePodByName(name string, pods []*corev1.Pod) []*corev1.Pod {
	for i, p := range pods {
		if p.Name == name {
			return append(pods[:i], pods[i+1:]...)
		}
	}
	return pods
}

// newController creates a new rollover controller with the specified config
func (f *fixture) newController(conf *config.Config) (*Controller, kubeinformers.SharedInformerFactory) {
	f.kubeclient = k8sfake.NewSimpleClientset()

	podInformerFactory := createRolloverIndexPodInformerFactory(
		f.kubeclient,
		conf.FlagRolloverIndexLabelKey,
		conf.FlagRolloverIndexAnnotationKey,
		conf.FlagNamespaceScope,
	)

	done := make(chan struct{})
	c := NewController(f.kubeclient,
		podInformerFactory.Core().V1().Pods(), conf, done)

	c.recorder = &record.FakeRecorder{}

	for _, p := range f.podLister {
		err := podInformerFactory.Core().V1().Pods().Informer().GetIndexer().Add(p)
		assert.NoError(f.t, err)
	}

	return c, podInformerFactory
}

// runControlLoop runs the controller logic once and returns a slice of performed actions from elastic and kibana
func (f *fixture) runControlLoop(conf *config.Config, podName string) ([]string, []string) {
	c, i := f.newController(conf)
	stopCh := make(chan struct{})
	defer close(stopCh)
	i.Start(stopCh)

	err := c.syncHandler(podName)
	if err != nil {
		f.t.Errorf("error syncing pod: %v", err)
	}

	kbPerformedActions := []string{}
	if c.kbClient != nil {
		kbPerformedActions = c.kbClient.PerformedActions
	}

	esPerformedActions := []string{}
	if c.esClient != nil {
		esPerformedActions = c.esClient.PerformedActions
	}

	return esPerformedActions, kbPerformedActions
}

func (f *fixture) ensureDockerNetworkExists() error {
	resp, err := f.dockerClient.NetworkList(
		context.Background(),
		types.NetworkListOptions{Filters: filters.NewArgs(filters.Arg("name", "elastic"))},
	)
	if err != nil {
		return err
	}
	if len(resp) == 0 {
		_, err := f.dockerClient.NetworkCreate(context.Background(), "elastic", types.NetworkCreate{})
		if err != nil {
			return err
		}
	}
	return nil
}

// createTestStack creates a elasticsearch instance in docker, exposes the ports and returns a config that connects to it
func (f *fixture) createTestStack(version string) (*config.Config, func(), error) {
	dockerImageBase := "docker.elastic.co"

	containerHost := "http://localhost"
	containerHostEnv := os.Getenv("ES_HOST")

	if containerHostEnv != "" {
		containerHost = containerHostEnv
	}

	conf := config.InitApp(true)
	conf.FlagESHost = containerHost
	conf.FlagESPort = strconv.Itoa(9000 + rand.Intn(999))
	kbPort := strconv.Itoa(55555 + rand.Intn(999))
	conf.FlagKibanaURL = fmt.Sprintf("%s:%s", containerHost, kbPort)

	err := f.ensureDockerNetworkExists()
	if err != nil {
		return nil, func() {}, err
	}

	// pull the image
	elasticImage := fmt.Sprintf("%s/elasticsearch/elasticsearch:%s", dockerImageBase, version)
	reader, err := f.dockerClient.ImagePull(context.Background(), elasticImage, types.ImagePullOptions{})
	if err != nil {
		return nil, func() {}, err
	}

	_, err = io.Copy(os.Stdout, reader)
	assert.NoError(f.t, err)

	_, exposedPorts, _ := nat.ParsePortSpecs([]string{
		fmt.Sprintf("0.0.0.0:%s:9200/tcp", conf.FlagESPort),
	})

	containerConfig := container.Config{
		Image: elasticImage,
		Env: []string{
			// disable xpack
			"xpack.security.enabled=false",
			"discovery.type=single-node",
		},
	}

	testESInstanceName := fmt.Sprintf("testESInstance-%.3d", rand.Intn(999))

	cont, err := f.dockerClient.ContainerCreate(
		context.Background(),
		&containerConfig,
		&container.HostConfig{PortBindings: exposedPorts, Resources: container.Resources{CPUCount: int64(4)}},
		&network.NetworkingConfig{
			EndpointsConfig: map[string]*network.EndpointSettings{
				"elastic": {},
			},
		},
		testESInstanceName,
	)
	if err != nil {
		return nil, func() {}, err
	}

	cleanupFunc := func() {
		f.cleanupTestInstances(cont.ID)
	}

	err = f.dockerClient.ContainerStart(context.Background(), cont.ID, types.ContainerStartOptions{})
	if err != nil {
		return nil, cleanupFunc, err
	}

	err = waitForES(containerHost, conf.FlagESPort)
	if err != nil {
		return nil, cleanupFunc, err
	}

	// pull the kibana image
	kibanaImage := fmt.Sprintf("%s/kibana/kibana:%s", dockerImageBase, version)
	reader, err = f.dockerClient.ImagePull(
		context.Background(),
		kibanaImage,
		types.ImagePullOptions{},
	)
	if err != nil {
		return nil, cleanupFunc, err
	}
	_, err = io.Copy(os.Stdout, reader)
	assert.NoError(f.t, err)

	_, kibanaExposedPorts, _ := nat.ParsePortSpecs([]string{
		fmt.Sprintf("0.0.0.0:%s:5601/tcp", kbPort),
	})

	kibanaContainerConfig := container.Config{
		Image: kibanaImage,
		Env: []string{
			// disable interactive setup
			"INTERACTIVESETUP_ENABLED=false",
			fmt.Sprintf("ELASTICSEARCH_HOSTS=http://%s:%d", testESInstanceName, 9200),
		},
	}

	kibanaCont, err := f.dockerClient.ContainerCreate(
		context.Background(),
		&kibanaContainerConfig,
		&container.HostConfig{PortBindings: kibanaExposedPorts, Resources: container.Resources{CPUCount: int64(4)}},
		&network.NetworkingConfig{
			EndpointsConfig: map[string]*network.EndpointSettings{
				"elastic": {},
			},
		},
		fmt.Sprintf("testKBInstance-%.3d", rand.Intn(999)),
	)
	if err != nil {
		return nil, cleanupFunc, err
	}

	cleanupFunc = func() {
		f.cleanupTestInstances(cont.ID, kibanaCont.ID)
	}

	err = f.dockerClient.ContainerStart(context.Background(), kibanaCont.ID, types.ContainerStartOptions{})
	if err != nil {
		return nil, cleanupFunc, err
	}

	err = waitForKB(conf.FlagKibanaURL)
	if err != nil {
		return nil, cleanupFunc, err
	}

	return conf, cleanupFunc, nil
}

// waitForKB waits for a kibana instance to become available
func waitForKB(kbURL string) error {
	for {
		kb, err := kbutil.NewKBClient(
			&config.Config{FlagKibanaURL: kbURL},
			instrumentation.NewMetricsCollector("test"),
		)
		if err != nil {
			// we ignore the error for tests here when it stems from the version constraint
			fmt.Printf("error initializing kibana: %s\n", err)
		}

		err = kb.Status()
		if err != nil {
			fmt.Printf("kibana not yet ready: %s\n", err)
			time.Sleep(time.Second * 5)
			continue
		}
		fmt.Println("kibana is ready")
		return nil
	}
}

// waitForES waits for an elasticseach instance to become available
func waitForES(esHost, esPort string) error {
	cfg := elasticsearch.Config{
		Addresses: []string{
			fmt.Sprintf("%s:%s", esHost, esPort),
		},
	}

	client, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return err
	}

	for {
		_, err = client.Ping()
		if err != nil {
			fmt.Printf("ES not yet ready: %s\n", err)
			time.Sleep(time.Second * 5)
		} else {
			break
		}
	}
	return nil
}

// cleanupTestInstance removes an docker container by its ID for cleaning up test ressources
func (f *fixture) cleanupTestInstances(containerID ...string) {
	for _, id := range containerID {
		if id != "" {
			err := f.dockerClient.ContainerRemove(
				context.Background(),
				id,
				types.ContainerRemoveOptions{Force: true},
			)
			assert.NoError(f.t, err)
		}
	}
}
