# Default values for es-rollover-controller.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

#
# es-rollover-controller specific configuration
#

# controllerID -- ID of this es-rollover-controller instance. Adapt this if you have multiple controllers connected to the same elasticsearch cluster.
controllerID: "es-rollover-controller"

elasticsearch:
  # elasticsearch.host -- The elasticsearch host.
  host: http://elastic  
  # elasticsearch.port -- The elasticsearch port.
  port: 9200
  # elasticsearch.username -- The elasticsearch username.
  username: ""
  password:
    secretKeyRef:
       # elasticsearch.password.secretKeyRef.name -- Name of an existing secret containing the elasticsearch password. Alternatively specify `elasticsearch.password.raw`.
      name: ""
      # elasticsearch.password.secretKeyRef.key -- Key inside an existing secret which contains the elasticsearch password. `elasticsearch.password.raw`.
      key: ""
      # elasticsearch.password.raw -- Elasticsearch password, will create a matching Kubernetes secret. Alternatively specify `elasticsearch.password.secretKeyRef`.
    raw: ""
  # caCertSecretName -- Name of an existing secret containing the elasticsearch ca certificate.
  caCertSecretName: ""
  caCertFile: /usr/share/elasticsearch-certs/tls.crt
  # timeout -- The elasticsearch response header / dialer timeout.
  timeout: "30s"

kibana:
  # kibana.url -- The kibana API Base URL.
  url: http://kibana:5601
  # caCertSecretName -- Name of an existing secret containing the kibana ca certificate.
  caCertSecretName: ""
  caCertFile: /usr/share/kibana-certs/tls.crt

# createDataViews -- EXPERIMENTAL: Specifies whether data views should be created automatically in Kibana. Requires the kibana.url to be specified.
createDataViews: false

customILMPolicy:
  # customILMPolicy.configMapName -- Name of an existing configmap containing a custom ILM policy.
  configMapName: ""
  # customILMPolicy.configMapKey -- Key inside an existing configmap which contains a custom ILM policy.
  configMapKey: ""

# ilmPolicyName -- Name of the ILM policy that should be used.
ilmPolicyName: "es-rollover-controller"
# ilmPolicyLabelKey -- Optional key of the pod label that contains the name of an existing custom ILM policy that should be used for the index template. If not specified, the default ILM policy will be used.
ilmPolicyLabelKey: "elastic-ilm-policy"
# ilmFormat -- Format String of the rollover index, %s will be substituted with the index name.
ilmFormat: "%s-000001"
# ilmPolicyOverwrite -- Specifies whether the lifecycle policy is overwritten at startup.
ilmPolicyOverwrite: false
# cleanupLegacyIndexTemplates -- Whether to remove outdated legacy index templates, required if this controller was deployed pre 7.8.0
cleanupLegacyIndexTemplates: false
# garbageCollectionInterval -- Interval at which the garbage collection process should run to cleanup stale rollover artefacts, which are no longer managed by the controller. Requires elasticsearch v7.8.0+. A duration of e.g. 0m disables this feature.
garbageCollectionInterval: "15m"
# writeAliasFormat -- Format String of the write alias, %s will be substituted with the index name.
writeAliasFormat: "%s_writealias"
# indexTemplateFormat -- Format String of the index template, %s will be substituted with the index name..
indexTemplateFormat: "%s_es-rollover-controller"
# rolloverIndexLabelKey -- Key of the pod label that contains the name of the rollover index.
rolloverIndexLabelKey: "elastic-index"
# rolloverIndexAnnotationKey -- Key of the pod annotation that contains the name of the rollover index. Usage of rollover_index_label_key preferred for performance reasons, see README for more info.
rolloverIndexAnnotationKey: ""
# componentTemplatesAnnotationKey -- Key of the pod annotation that contains a comma separated list of component templates that will get merged in the index template in order.
componentTemplatesAnnotationKey: "elastic-component-templates"

namespaceScope: ""

log:
  # log.formatter -- The log formatter. Should be one of [json, text].
  formatter: "json"
  # log.rolloverIndexLabelValue -- The rolloverIndexLabel to set on the controller itself to forward and rollover its logs. If empty, no label will be set.
  rolloverIndexLabelValue: "es-rollover-controller"

    # exposePrometheusMetrics -- Whether to expose prometheus metrics on :9102/metrics
exposePrometheusMetrics: false

# cacheTTL -- TTL of cached annotations values that have already been discovered.
cacheTTL: "12h"
# workers -- Number of workers that will process kubernetes events.
workers: 2

vpa:
  # vpa.enabled -- Whether a vertical pod autoscaling CRD should be created for the controller
  enabled: false
  # vpa.updateMode -- The update mode for the vertical pod autoscaler
  updateMode: "Auto"

#
# General Kubernetes Configuration
#

replicaCount: 1
image:
  repository: msvechla/es-rollover-controller
  tag: "0.12.0"
  pullPolicy: IfNotPresent

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

resources: 
  limits:
    cpu: 100m
    memory: 128Mi
  requests:
    cpu: 100m
    memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}
