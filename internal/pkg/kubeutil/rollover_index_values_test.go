package kubeutil

import (
	"reflect"
	"testing"

	corev1 "k8s.io/api/core/v1"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
)

func Test_GetRolloverIndexKVForPod(t *testing.T) {
	type args struct {
		annotations map[string]string
		labels      map[string]string
		c           *config.Config
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{name: "label", args: args{labels: map[string]string{"elastic-index": "testlabel", "testlabel": "wasd"}, c: &config.Config{FlagRolloverIndexLabelKey: "elastic-index", FlagRolloverIndexAnnotationKey: "elastic-index"}}, want: map[string]string{"elastic-index": "testlabel"}},
		{name: "annotation", args: args{annotations: map[string]string{"elastic-index": "testannotation", "test": "wasd"}, c: &config.Config{FlagRolloverIndexLabelKey: "elastic-index", FlagRolloverIndexAnnotationKey: "elastic-index"}}, want: map[string]string{"elastic-index": "testannotation"}},
		{name: "annotation-label", args: args{labels: map[string]string{"elastic-index-label": "testlabel", "testlabel": "wasd"}, annotations: map[string]string{"elastic-index-annotation": "testannotation", "test": "wasd"}, c: &config.Config{FlagRolloverIndexLabelKey: "elastic-index-label", FlagRolloverIndexAnnotationKey: "elastic-index-annotation"}}, want: map[string]string{"elastic-index-label": "testlabel", "elastic-index-annotation": "testannotation"}},
		{name: "label-precedence", args: args{labels: map[string]string{"elastic-index": "testlabel", "testlabel": "wasd"}, annotations: map[string]string{"elastic-index": "testannotation", "test": "wasd"}, c: &config.Config{FlagRolloverIndexLabelKey: "elastic-index", FlagRolloverIndexAnnotationKey: "elastic-index"}}, want: map[string]string{"elastic-index": "testlabel"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pod := corev1.Pod{}
			pod.Annotations = tt.args.annotations
			pod.Labels = tt.args.labels

			if got := GetRolloverIndexKVForPod(&pod, tt.args.c); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetRolloverIndexKVforPod() = %v, want %v", got, tt.want)
			}
		})
	}
}
