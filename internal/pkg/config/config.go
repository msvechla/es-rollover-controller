// Package config contains structs to store runtime configuration
package config

import (
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/whuang8/redactrus"

	"github.com/urfave/cli/v2"
)

const (
	AppVersion = "v0.12.0"
	logFormatterText = "text"
	logFormatterJSON = "json"
	// ILMIncrementPattern is the -000001 suffix at the end of the ILM format
	ILMIncrementPattern = "-000001"
)

// Config contains the runtime configuration paresed from CLI flags or environment variables
type Config struct {
	// general configuration
	FlagLogFormatter              string
	FlagNamespaceScope            string
	FlagCacheTTL                  string
	FlagWorkers                   int
	FlagControllerID              string
	FlagGarbageCollectionInterval string
	FlagExposePrometheusMetrics   bool

	// kubernetes configuration
	FlagKubeconfig string
	FlagMasterURL  string

	// elasticsearch configuration
	FlagESHost       string
	FlagESPort       string
	FlagESUsername   string
	FlagESPassword   string
	FlagESCACertFile string
	FlagESTimeout    string

	// kibana configuration
	FlagKibanaURL        string
	FlagKibanaCACertFile string

	FlagCreateDataViews bool

	FlagRolloverIndexLabelKey           string
	FlagRolloverIndexAnnotationKey      string
	FlagComponentTemplatesAnnotationKey string
	FlagILMPolicyName                   string
	FlagILMPolicyFile                   string
	FlagILMPolicyLabelKey               string
	FlagILMFormat                       string
	FlagILMPolicyOverwrite              bool
	FlagWriteAliasFormat                string
	FlagIndexTemplateFormat             string
	FlagCleanupLegacyIndexTemplates     bool
}

// InitApp creates the cli application to read all flags from the environment and returns a config
func InitApp(ignoreErrors bool) *Config {
	// create our custom config to store runtime configuration
	config := Config{}

	app := &cli.App{
		Name:    "es-rollover-controller",
		Usage:   "automating dynamic elasticsearch index lifecycle management",
		Version: AppVersion,
		Action: func(c *cli.Context) error {
			initLogRedaction(&config)

			log.WithField("version", AppVersion).
				Infof("Running es-rollover-controller with config:\n%s", configHelptext(c))
			if err := config.EnsureValidFlags(); err != nil {
				return fmt.Errorf("Error validating CLI Flags: %s", err)
			}
			return nil
		},
	}

	// initialize the global flags
	initGlobalFlags(app, &config)

	// initialize the elasticsearch flags
	initESFlags(app, &config)

	// initialize the kibana flags
	initKibanaFlags(app, &config)

	err := app.Run(os.Args)
	if err != nil {
		if !ignoreErrors {
			log.Fatal(err)
		}
	}

	initLogging(&config)

	return &config
}

func initGlobalFlags(app *cli.App, config *Config) {
	generalFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "kubeconfig",
			Value:       "",
			Usage:       "Path to a kubeconfig. Only required if out-of-cluster.",
			EnvVars:     []string{"KUBECONFIG"},
			Destination: &config.FlagKubeconfig,
		},
		&cli.StringFlag{
			Name:        "master",
			Value:       "",
			Usage:       "The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster.",
			EnvVars:     []string{"MASTER"},
			Destination: &config.FlagMasterURL,
		},
		&cli.StringFlag{
			Name:        "cache_ttl",
			Value:       "12h",
			Usage:       "TTL of cached annotations values that have already been discovered.",
			EnvVars:     []string{"CACHE_TTL"},
			Destination: &config.FlagCacheTTL,
		},
		&cli.StringFlag{
			Name:  "log_formatter",
			Value: logFormatterText,
			Usage: fmt.Sprintf(
				"The log formatter. Should be one of [%s, %s].",
				logFormatterJSON,
				logFormatterText,
			),
			EnvVars:     []string{"LOG_FORMATTER"},
			Destination: &config.FlagLogFormatter,
		},
		&cli.StringFlag{
			Name:        "namespace_scope",
			Value:       "",
			Usage:       "The namespace which the controller should watch. If empty, the controller watches all namespaces.",
			EnvVars:     []string{"NAMESPACE_SCOPE"},
			Destination: &config.FlagNamespaceScope,
		},
		&cli.IntFlag{
			Name:        "workers",
			Value:       2,
			Usage:       "Number of workers that will process kubernetes events.",
			EnvVars:     []string{"WORKERS"},
			Destination: &config.FlagWorkers,
		},
		&cli.StringFlag{
			Name:        "controller_id",
			Value:       "es-rollover-controller",
			Usage:       "ID of this es-rollover-controller instance. Adapt this if you have multiple controllers connected to the same elasticsearch cluster.",
			EnvVars:     []string{"CONTROLLER_ID"},
			Destination: &config.FlagControllerID,
		},
		&cli.StringFlag{
			Name:        "garbage_collection_interval",
			Value:       "15m",
			Usage:       "Interval at which the garbage collection process should run to cleanup stale rollover artefacts, which are no longer managed by the controller. Requires elasticsearch v7.8.0+. A duration of e.g. 0m disables this feature.",
			EnvVars:     []string{"GARBAGE_COLLECTION_INTERVAL"},
			Destination: &config.FlagGarbageCollectionInterval,
		},
		&cli.BoolFlag{
			Name:        "expose_prometheus_metrics",
			Value:       false,
			Usage:       "Whether to expose prometheus metrics on :9102/metrics",
			EnvVars:     []string{"EXPOSE_PROMETHEUS_METRICS"},
			Destination: &config.FlagExposePrometheusMetrics,
		},
	}

	app.Flags = append(app.Flags, generalFlags...)
}

func initKibanaFlags(app *cli.App, config *Config) {
	kibanaFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "kibana_url",
			Value:       "http://kibana:5601",
			Usage:       "The Kibana API URL.",
			EnvVars:     []string{"KIBANA_URL"},
			Destination: &config.FlagKibanaURL,
		},
		&cli.StringFlag{
			Name:        "kibana_ca_cert_file",
			Value:       "",
			Usage:       "Path to the Kibana CA certificate file.",
			EnvVars:     []string{"KIBANA_CA_CERT_FILE"},
			Destination: &config.FlagKibanaCACertFile,
		},
		&cli.BoolFlag{
			Name:        "create_data_views",
			Value:       false,
			Usage:       "EXPERIMENTAL: Specifies whether data views should be created automatically in Kibana.",
			EnvVars:     []string{"CREATE_DATA_VIEWS"},
			Destination: &config.FlagCreateDataViews,
		},
	}

	app.Flags = append(app.Flags, kibanaFlags...)
}

// initESFlags initialzes the elasticsearch related cli flags
func initESFlags(app *cli.App, config *Config) {
	esFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "elasticsearch_host",
			Value:       "http://elastic",
			Usage:       "The elasticsearch host.",
			EnvVars:     []string{"ELASTICSEARCH_HOST"},
			Destination: &config.FlagESHost,
		},
		&cli.StringFlag{
			Name:        "elasticsearch_port",
			Value:       "9200",
			Usage:       "The elasticsearch port.",
			EnvVars:     []string{"ELASTICSEARCH_PORT"},
			Destination: &config.FlagESPort,
		},
		&cli.StringFlag{
			Name:        "elasticsearch_username",
			Value:       "",
			Usage:       "The elasticsearch username.",
			EnvVars:     []string{"ELASTICSEARCH_USERNAME"},
			Destination: &config.FlagESUsername,
		},
		&cli.StringFlag{
			Name:        "elasticsearch_password",
			Value:       "",
			Usage:       "The elasticsearch password.",
			EnvVars:     []string{"ELASTICSEARCH_PASSWORD"},
			Destination: &config.FlagESPassword,
		},
		&cli.StringFlag{
			Name:        "elasticsearch_ca_cert_file",
			Value:       "",
			Usage:       "Path to the elasticsearch ca certificate file used to connect to the cluster.",
			EnvVars:     []string{"ELASTICSEARCH_CA_CERT_FILE"},
			Destination: &config.FlagESCACertFile,
		},
		&cli.StringFlag{
			Name:        "elasticsearch_timeout",
			Value:       "30s",
			Usage:       "The elasticsearch response header / dialer timeout.",
			EnvVars:     []string{"ELASTICSEARCH_TIMEOUT"},
			Destination: &config.FlagESTimeout,
		},
		&cli.StringFlag{
			Name:        "rollover_index_label_key",
			Value:       "elastic-index",
			Usage:       "Key of the pod label that contains the name of the rollover index. Takes precedence over rollover_index_annotation_key.",
			EnvVars:     []string{"ROLLOVER_INDEX_LABEL_KEY"},
			Destination: &config.FlagRolloverIndexLabelKey,
		},
		&cli.StringFlag{
			Name:        "rollover_index_annotation_key",
			Value:       "",
			Usage:       "Key of the pod annotation that contains the name of the rollover index. Usage of rollover_index_label_key preferred for performance reasons, see README for more info.",
			EnvVars:     []string{"ROLLOVER_INDEX_ANNOTATION_KEY"},
			Destination: &config.FlagRolloverIndexAnnotationKey,
		},
		&cli.StringFlag{
			Name:        "component_templates_annotation_key",
			Value:       "elastic-component-templates",
			Usage:       "Key of the pod annotation that contains a comma separated list of component templates that will get merged in the index template in order.",
			EnvVars:     []string{"COMPONENT_TEMPLATES_ANNOTATION_KEY"},
			Destination: &config.FlagComponentTemplatesAnnotationKey,
		},
		&cli.StringFlag{
			Name:        "write_alias_format",
			Value:       "%s_writealias",
			Usage:       "Format String of the write alias, %s will be substituted with the index name.",
			EnvVars:     []string{"WRITE_ALIAS_FORMAT"},
			Destination: &config.FlagWriteAliasFormat,
		},
		&cli.StringFlag{
			Name:        "index_template_format",
			Value:       "%s_es-rollover-controller",
			Usage:       "Format String of the index template, %s will be substituted with the index name.",
			EnvVars:     []string{"INDEX_TEMPLATE_FORMAT"},
			Destination: &config.FlagIndexTemplateFormat,
		},
		&cli.BoolFlag{
			Name:        "cleanup_legacy_index_templates",
			Value:       false,
			Usage:       "Whether to remove outdated legacy index templates, required if this controller was deployed pre 7.8.0",
			EnvVars:     []string{"CLEANUP_LEGACY_INDEX_TEMPLATES"},
			Destination: &config.FlagCleanupLegacyIndexTemplates,
		},
		&cli.StringFlag{
			Name:        "ilm_policy_name",
			Value:       "es-rollover-controller",
			Usage:       "Name of the ILM policy that should be used.",
			EnvVars:     []string{"ILM_POLICY_NAME"},
			Destination: &config.FlagILMPolicyName,
		},
		&cli.StringFlag{
			Name:        "ilm_policy_file",
			Value:       "",
			Usage:       "Path to the ilm policy file that should be used.",
			EnvVars:     []string{"ILM_POLICY_FILE"},
			Destination: &config.FlagILMPolicyFile,
		}, &cli.StringFlag{
			Name:        "ilm_policy_label_key",
			Value:       "elastic-ilm-policy",
			Usage:       "Optional key of the pod label that contains the name of the ilm policy to attach. If no label is found the default ILM policy will be attached.",
			EnvVars:     []string{"ILM_POLICY_LABEL_KEY"},
			Destination: &config.FlagILMPolicyLabelKey,
		},
		&cli.StringFlag{
			Name:        "ilm_format",
			Value:       "%s-000001",
			Usage:       "Format String of the rollover index, %s will be substituted with the index name. Has to end with -000001.",
			EnvVars:     []string{"ILM_FORMAT"},
			Destination: &config.FlagILMFormat,
		},
		&cli.BoolFlag{
			Name:        "ilm_policy_overwrite",
			Value:       false,
			Usage:       "Specifies whether the lifecycle policy is overwritten at startup.",
			EnvVars:     []string{"ILM_POLICY_OVERWRITE"},
			Destination: &config.FlagILMPolicyOverwrite,
		},
	}

	app.Flags = append(app.Flags, esFlags...)
}

// initLogging initializes the logging framework
func initLogging(config *Config) {
	switch config.FlagLogFormatter {
	case logFormatterJSON:
		log.SetFormatter(&log.JSONFormatter{})
	case logFormatterText:
		log.SetFormatter(&log.TextFormatter{})
	default:
		log.Fatalf("Log formatter should be one of [%s, %s].", logFormatterJSON, logFormatterText)
	}
}

// initLogRedaction redacts sensitive values from the logs
func initLogRedaction(config *Config) {
	redactedValues := []string{}

	if config.FlagESPassword != "" {
		redactedValues = append(redactedValues, config.FlagESPassword)
	}

	rh := &redactrus.Hook{
		AcceptedLevels: log.AllLevels,
		RedactionList:  redactedValues,
	}

	log.AddHook(rh)
}

// EnsureValidFlags ensures only valid flags are configured
func (c *Config) EnsureValidFlags() error {
	if !strings.HasSuffix(strings.Trim(c.FlagILMFormat, "<>"), ILMIncrementPattern) {
		return fmt.Errorf("flag ILMFormat should contain suffix %s, got %s", ILMIncrementPattern, c.FlagILMFormat)
	}

	if _, err := time.ParseDuration(c.FlagCacheTTL); err != nil {
		return fmt.Errorf("error parsing duration of flag CacheTTL: %s", err)
	}

	if _, err := time.ParseDuration(c.FlagESTimeout); err != nil {
		return fmt.Errorf("error parsing duration of flag ESTimeout: %s", err)
	}

	if _, err := time.ParseDuration(c.FlagGarbageCollectionInterval); err != nil {
		return fmt.Errorf("error parsing duration of flag GarbageCollectionInterval: %s", err)
	}

	return nil
}

func configHelptext(c *cli.Context) string {
	sb := strings.Builder{}

	for _, f := range c.App.Flags {
		_, err := sb.WriteString(fmt.Sprintf("%s\n", f.String()))
		if err != nil {
			log.Fatal(err)
		}
	}
	return sb.String()
}
