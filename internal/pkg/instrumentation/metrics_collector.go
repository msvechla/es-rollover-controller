package instrumentation

import (
	"net/http"

	"github.com/jellydator/ttlcache/v2"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	namespace           = "esrc"
	reconcilerSubsystem = "reconciler"
)

// MetricsCollector contains all data that will be collected
type MetricsCollector struct {
	registerer            prometheus.Registerer
	cacheNumItems         prometheus.Gauge
	cacheNumEvicted       prometheus.Gauge
	cacheNumHits          prometheus.Gauge
	cacheNumMisses        prometheus.Gauge
	cacheNumInserted      prometheus.Gauge
	cacheNumRetrievals    prometheus.Gauge
	actionsPerformedTotal *prometheus.CounterVec
}

// NewMetricsCollector creates a new metrics collector and registers all metrics
func NewMetricsCollector(controllerID string) *MetricsCollector {
	mc := MetricsCollector{
		cacheNumItems: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_items",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of items in the cache",
		}),
		cacheNumHits: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_hits",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of cache hits",
		}),
		cacheNumMisses: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_misses",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of cache misses",
		}),
		cacheNumEvicted: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_evicted",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of cache evictions",
		}),
		cacheNumInserted: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_inserted",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of objects inserted to the cache",
		}),
		cacheNumRetrievals: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "cache_num_retrievals",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The currrent number of objects retrieved from the cache",
		}),
		actionsPerformedTotal: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:      "actions_performed_total",
			Subsystem: reconcilerSubsystem,
			Namespace: namespace,
			Help:      "The total number of actions performed",
		}, []string{"action"}),
		registerer: prometheus.WrapRegistererWith(prometheus.Labels{"controllerID": controllerID}, prometheus.DefaultRegisterer),
	}

	collectors := []prometheus.Collector{
		mc.cacheNumItems,
		mc.cacheNumHits,
		mc.cacheNumMisses,
		mc.cacheNumEvicted,
		mc.cacheNumRetrievals,
		mc.cacheNumInserted,
		mc.actionsPerformedTotal,
	}

	// unregister all collectors first to avoid duplicate registration e.g. in tests
	for _, c := range collectors {
		mc.registerer.Unregister(c)
	}

	// register all collectors
	mc.registerer.MustRegister(collectors...)

	return &mc
}

func (m *MetricsCollector) ObserveCacheMetics(cache *ttlcache.Cache) {
	m.cacheNumItems.Set(float64(cache.Count()))
	m.cacheNumHits.Set(float64(cache.GetMetrics().Hits))
	m.cacheNumMisses.Set(float64(cache.GetMetrics().Misses))
	m.cacheNumEvicted.Set(float64(cache.GetMetrics().Evicted))
	m.cacheNumRetrievals.Set(float64(cache.GetMetrics().Retrievals))
	m.cacheNumInserted.Set(float64(cache.GetMetrics().Inserted))
}

func (m *MetricsCollector) ObservePerformedAction(action string) {
	m.actionsPerformedTotal.WithLabelValues(action).Inc()
}

// ServeMetrics serves the prometheus metrics so they can get scraped
func (m *MetricsCollector) ServeMetrics() {
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":9102", nil)
}
