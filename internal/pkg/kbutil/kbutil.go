package kbutil

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strings"

	"github.com/Masterminds/semver"
	log "github.com/sirupsen/logrus"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/instrumentation"
)

const (
	// ActionCreateDataView indicates that the controller created a kibana data view
	ActionCreateDataView = "CreateDataView"
	// ActionUpdateDataView indicates that the controller updated a kibana data view
	ActionUpdateDataView = "UpdateDataView"
)

// DataView is the kibana data view object
type DataView struct {
	ID            *string `json:"id,omitempty"`
	Title         *string `json:"title,omitempty"`
	Name          *string `json:"name,omitempty"`
	AllowNoIndex  *bool   `json:"allowNoIndex,omitempty"`
	TimeFieldName *string `json:"timeFieldName,omitempty"`
}

// supportedKibanaVersion is the minimum version of Kibana that supports all required data view APIs
var supportedKibanaVersion, _ = semver.NewConstraint(">= 8.4.0")

// KBClient is used to interact with the Kibana API
type KBClient struct {
	username         string
	password         string
	url              string
	ctx              context.Context
	PerformedActions []string
	version          *semver.Version
	metricsCollector *instrumentation.MetricsCollector
	httpClient       *http.Client
}

// NewKBClient creates a new Kibana client
func NewKBClient(config *config.Config, metricsCollector *instrumentation.MetricsCollector) (*KBClient, error) {
	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		log.Fatal(err)
	}

	if config.FlagKibanaCACertFile != "" {
		caCert, err := ioutil.ReadFile(config.FlagKibanaCACertFile)
		if err != nil {
			log.Fatal(err)
		}
		caCertPool.AppendCertsFromPEM(caCert)
	}

	// Create a new transport that uses our CA pool
	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			MinVersion:         tls.VersionTLS11,
			RootCAs:            caCertPool,
			InsecureSkipVerify: false,
		},
	}

	// Create a new client that uses our transport
	httpClient := &http.Client{Transport: transport}

	kbClient := &KBClient{
		username:         config.FlagESUsername,
		password:         config.FlagESPassword,
		url:              config.FlagKibanaURL,
		ctx:              context.Background(),
		metricsCollector: metricsCollector,
		httpClient:       httpClient,
	}

	if err := kbClient.Status(); err != nil {
		return kbClient, fmt.Errorf("determining Kibana status: %w", err)
	}

	version, err := kbClient.fetchVersion()
	if err != nil {
		return kbClient, fmt.Errorf("fetching Kibana version: %w", err)
	}
	kbClient.version = version

	if hasIndexPatternSupport, _ := kbClient.hasDataViewSupport(); !hasIndexPatternSupport {
		return kbClient, fmt.Errorf(
			"kibana version %s does not support all required data view APIs, please disable the feature in the controller config",
			version,
		)
	}

	return kbClient, nil
}

// hasDataViewSupport returns true if the kibana instance supports the data view APIs
func (kb *KBClient) hasDataViewSupport() (bool, error) {
	if kb.version == nil {
		version, err := kb.fetchVersion()
		if err != nil {
			return false, err
		}
		kb.version = version
	}
	return supportedKibanaVersion.Check(kb.version), nil
}

// fetchVersion fetches the version of the kibana instance by using the /api/status endpoint
func (kb *KBClient) fetchVersion() (*semver.Version, error) {
	statusURL, err := url.JoinPath(kb.url, "/api/status")
	if err != nil {
		return nil, err
	}

	req, _ := http.NewRequest("GET", statusURL, nil)
	req.SetBasicAuth(kb.username, kb.password)
	req.Header.Set("kbn-xsrf", "true")

	resp, err := kb.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error checking kibana status: %s", resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type StatusResponse struct {
		Version struct {
			Number string `json:"number"`
		} `json:"version"`
	}
	var statusResponse StatusResponse
	if err := json.Unmarshal(body, &statusResponse); err != nil {
		return nil, err
	}

	version, err := semver.NewVersion(statusResponse.Version.Number)
	if err != nil {
		return nil, err
	}
	return version, nil
}

// Status returns no error when the kibana /api/status returns status code 200
func (kb *KBClient) Status() error {
	statusURL, err := url.JoinPath(kb.url, "/api/status")
	if err != nil {
		return err
	}

	req, _ := http.NewRequest("GET", statusURL, nil)
	req.SetBasicAuth(kb.username, kb.password)
	req.Header.Set("kbn-xsrf", "true")

	resp, err := kb.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error checking kibana status: %s", resp.Status)
	}
	return nil
}

func dataViewID(pattern string) string {
	return fmt.Sprintf("esrc-%s", pattern)
}

// DataViewExistsAndIsUpToDate returns true if the data view exists and is up to date
func (kb *KBClient) DataViewExistsAndIsUpToDate(pattern string) (bool, bool, error) {
	dataViewURL, err := url.JoinPath(
		kb.url,
		fmt.Sprintf("/api/data_views/data_view/%s", dataViewID(pattern)),
	)
	if err != nil {
		return false, false, err
	}
	req, _ := http.NewRequest("GET", dataViewURL, nil)
	req.SetBasicAuth(kb.username, kb.password)
	req.Header.Set("kbn-xsrf", "true")
	req.Header.Set("Content-Type", "application/json")

	resp, err := kb.httpClient.Do(req)
	if err != nil {
		return false, false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusNotFound {
		return false, false, nil
	}
	if resp.StatusCode != http.StatusOK {
		return false, false, fmt.Errorf("got status: %s", resp.Status)
	}

	// check if the data view is up to date
	type DataViewResponse struct {
		DataView DataView `json:"data_view"`
	}
	var dataViewResponse DataViewResponse
	if err := json.NewDecoder(resp.Body).Decode(&dataViewResponse); err != nil {
		return true, false, err
	}

	expectedDataView := expectedDataView(pattern)
	if reflect.DeepEqual(dataViewResponse.DataView, expectedDataView) {
		return true, true, nil
	}

	return true, false, nil
}

// EnsureDataViewExists ensures that a data view with the given pattern exists
func (kb *KBClient) EnsureDataViewExists(pattern string) error {
	exists, upToDate, err := kb.DataViewExistsAndIsUpToDate(pattern)
	if err != nil {
		return fmt.Errorf("checking data view existence: %w", err)
	}
	if exists && upToDate {
		return nil
	}

	if !exists {
		if err := kb.createDataView(pattern); err != nil {
			return err
		}
		return nil
	}

	if !upToDate {
		if err := kb.updateDataView(pattern); err != nil {
			return err
		}
	}

	return nil
}

// createDataView creates a data view with the given pattern
func (kb *KBClient) createDataView(pattern string) error {
	dataViewURL, err := url.JoinPath(kb.url, "/api/data_views/data_view")
	if err != nil {
		return err
	}

	type Request struct {
		DataView DataView `json:"data_view"`
	}

	dataViewBody := Request{
		expectedDataView(pattern),
	}

	dataViewJSON, _ := json.Marshal(dataViewBody)
	req, _ := http.NewRequest("POST", dataViewURL, bytes.NewBuffer(dataViewJSON))
	req.SetBasicAuth(kb.username, kb.password)
	req.Header.Set("kbn-xsrf", "true")
	req.Header.Set("Content-Type", "application/json")

	resp, err := kb.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	respBody, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("error creating data view %s: %s: %s", pattern, resp.Status, respBody)
	}

	if resp.StatusCode == http.StatusBadRequest && strings.Contains(strings.ToLower(string(respBody)), "duplicate") {
		log.Warn("data view already exists, not managed by the controller. Ignoring duplicate")
		return nil
	}

	kb.PerformedActions = append(kb.PerformedActions, ActionCreateDataView)
	kb.metricsCollector.ObservePerformedAction(ActionCreateDataView)
	return nil
}

// updateDataView updates a data view with the given pattern
func (kb *KBClient) updateDataView(pattern string) error {
	dataViewURL, err := url.JoinPath(
		kb.url,
		fmt.Sprintf("/api/data_views/data_view/%s", dataViewID(pattern)),
	)
	if err != nil {
		return err
	}

	type Request struct {
		DataView DataView `json:"data_view"`
	}

	dataViewBody := Request{
		expectedDataView(pattern),
	}

	// set non updateable fields to nil
	dataViewBody.DataView.ID = nil
	dataViewBody.DataView.AllowNoIndex = nil

	dataViewJSON, _ := json.Marshal(dataViewBody)
	req, _ := http.NewRequest("POST", dataViewURL, bytes.NewBuffer(dataViewJSON))
	req.SetBasicAuth(kb.username, kb.password)
	req.Header.Set("kbn-xsrf", "true")
	req.Header.Set("Content-Type", "application/json")

	resp, err := kb.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	respBody, _ := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("error updating data view %s: %s: %s", pattern, resp.Status, respBody)
	}

	kb.PerformedActions = append(kb.PerformedActions, ActionUpdateDataView)
	kb.metricsCollector.ObservePerformedAction(ActionUpdateDataView)
	return nil
}

// expectedDataView returns the expected data view for the given pattern
func expectedDataView(pattern string) DataView {
	return DataView{
		ID:            stringPtr(dataViewID(pattern)),
		Name:          stringPtr(dataViewID(pattern)),
		Title:         stringPtr(pattern),
		AllowNoIndex:  boolPtr(true),
		TimeFieldName: stringPtr("@timestamp"),
	}
}

func stringPtr(s string) *string {
	return &s
}

func boolPtr(b bool) *bool {
	return &b
}
