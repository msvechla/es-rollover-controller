package esutil

import (
	"testing"

	"github.com/Masterminds/semver"
	"github.com/stretchr/testify/assert"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
)

func TestGetRolloverIndex(t *testing.T) {
	testCases := []struct {
		labelValue     string
		expectedOutput string
		flagILMFormat  string
	}{
		{labelValue: "helloWorld", expectedOutput: "helloWorld-000001", flagILMFormat: "%s-000001"},
		{labelValue: "testcase", expectedOutput: "testcase-000001", flagILMFormat: "%s-000001"},
		{
			labelValue:     `test`,
			expectedOutput: "%3Clogs-elastic%5C%7BON%5C%7D-%7Bnow%2FM%7D-test-000001%3E",
			flagILMFormat:  `<logs-elastic\{ON\}-{now/M}-%s-000001>`,
		},
		{
			labelValue:     `helloWorld`,
			expectedOutput: "%3Ck8s-%7Bnow%2FM%7D-%7Bnow%2FM%7D-helloWorld-000001%3E",
			flagILMFormat:  `<k8s-{now/M}-{now/M}-%s-000001>`,
		},
	}

	cli := ESClient{config: &config.Config{}}

	for _, c := range testCases {
		cli.config.FlagILMFormat = c.flagILMFormat
		actualOutput := cli.GetRolloverIndex(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetIndexTemplateName(t *testing.T) {
	cli := ESClient{config: &config.Config{FlagIndexTemplateFormat: "%s_rac"}}
	testCases := []struct {
		labelValue     string
		expectedOutput string
	}{
		{labelValue: "helloWorld", expectedOutput: "helloWorld_rac"},
		{labelValue: "testcase", expectedOutput: "testcase_rac"},
	}

	for _, c := range testCases {
		actualOutput := cli.GetIndexTemplateName(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetWriteAlias(t *testing.T) {
	cli := ESClient{config: &config.Config{FlagWriteAliasFormat: "%s_racwrite"}}
	testCases := []struct {
		labelValue     string
		expectedOutput string
	}{
		{labelValue: "helloWorld", expectedOutput: "helloWorld_racwrite"},
		{labelValue: "testcase", expectedOutput: "testcase_racwrite"},
	}

	for _, c := range testCases {
		actualOutput := cli.GetWriteAlias(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetILMPolicy(t *testing.T) {
	const defaultPolicy = "my-default-policy"

	cli := ESClient{config: &config.Config{FlagILMPolicyName: defaultPolicy}}
	testCases := []struct {
		labelValue     string
		expectedOutput string
	}{
		{labelValue: "", expectedOutput: defaultPolicy},
		{labelValue: "awesomePolicy", expectedOutput: "awesomePolicy"},
	}

	for _, c := range testCases {
		actualOutput := cli.GetILMPolicyName(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestBaseIndexPatternPrefixFromILMFormat(t *testing.T) {
	cli := ESClient{}
	testCases := []struct {
		ilmFormat      string
		labelValue     string
		expectedOutput string
	}{
		{ilmFormat: "%s-000001", labelValue: "helloWorld", expectedOutput: "helloWorld"},
		{ilmFormat: "kube-%s-000001", labelValue: "mytestlog", expectedOutput: "kube-mytestlog"},
		{ilmFormat: "k8s-%s-logs-000001", labelValue: "yay", expectedOutput: "k8s-yay-logs"},
		{ilmFormat: "k8s.%s.logs-000001", labelValue: "yay", expectedOutput: "k8s.yay.logs"},
		{
			ilmFormat:      `<logs-elastic\{ON\}-{now/M}-%s-000001>`,
			labelValue:     "test",
			expectedOutput: `logs-elastic\{ON\}-*-test`,
		},
		{
			ilmFormat:      `<k8s-{now/M}-{now/M}-%s-000001>`,
			labelValue:     "test",
			expectedOutput: "k8s-*-*-test",
		},
	}

	for _, c := range testCases {
		cli.config = &config.Config{FlagILMFormat: c.ilmFormat}
		actualOutput := cli.baseIndexPatternPrefixFromILMFormat(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetRolloverIndexPattern(t *testing.T) {
	cli := ESClient{}
	testCases := []struct {
		ilmFormat      string
		labelValue     string
		expectedOutput []string
	}{
		{
			ilmFormat:  "%s-000001",
			labelValue: "helloWorld",
			expectedOutput: []string{
				"helloWorld-0*",
				"helloWorld-1*",
				"helloWorld-2*",
				"helloWorld-3*",
				"helloWorld-4*",
				"helloWorld-5*",
				"helloWorld-6*",
				"helloWorld-7*",
				"helloWorld-8*",
				"helloWorld-9*",
			},
		},
		{
			ilmFormat:  "kube-%s-000001",
			labelValue: "mytestlog",
			expectedOutput: []string{
				"kube-mytestlog-0*",
				"kube-mytestlog-1*",
				"kube-mytestlog-2*",
				"kube-mytestlog-3*",
				"kube-mytestlog-4*",
				"kube-mytestlog-5*",
				"kube-mytestlog-6*",
				"kube-mytestlog-7*",
				"kube-mytestlog-8*",
				"kube-mytestlog-9*",
			},
		},
		{
			ilmFormat:  "k8s-%s-logs-000001",
			labelValue: "yay",
			expectedOutput: []string{
				"k8s-yay-logs-0*",
				"k8s-yay-logs-1*",
				"k8s-yay-logs-2*",
				"k8s-yay-logs-3*",
				"k8s-yay-logs-4*",
				"k8s-yay-logs-5*",
				"k8s-yay-logs-6*",
				"k8s-yay-logs-7*",
				"k8s-yay-logs-8*",
				"k8s-yay-logs-9*",
			},
		},
		{
			ilmFormat:  "k8s.%s.logs-000001",
			labelValue: "yay",
			expectedOutput: []string{
				"k8s.yay.logs-0*",
				"k8s.yay.logs-1*",
				"k8s.yay.logs-2*",
				"k8s.yay.logs-3*",
				"k8s.yay.logs-4*",
				"k8s.yay.logs-5*",
				"k8s.yay.logs-6*",
				"k8s.yay.logs-7*",
				"k8s.yay.logs-8*",
				"k8s.yay.logs-9*",
			},
		},
		{
			ilmFormat:  `<logs-elastic\{ON\}-{now/M}-%s-000001>`,
			labelValue: "test",
			expectedOutput: []string{
				"logs-elastic%5C%7BON%5C%7D-*-test-0*",
				"logs-elastic%5C%7BON%5C%7D-*-test-1*",
				"logs-elastic%5C%7BON%5C%7D-*-test-2*",
				"logs-elastic%5C%7BON%5C%7D-*-test-3*",
				"logs-elastic%5C%7BON%5C%7D-*-test-4*",
				"logs-elastic%5C%7BON%5C%7D-*-test-5*",
				"logs-elastic%5C%7BON%5C%7D-*-test-6*",
				"logs-elastic%5C%7BON%5C%7D-*-test-7*",
				"logs-elastic%5C%7BON%5C%7D-*-test-8*",
				"logs-elastic%5C%7BON%5C%7D-*-test-9*",
			},
		},
		{
			ilmFormat:  `<k8s-{now/M}-{now/M}-%s-000001>`,
			labelValue: "test",
			expectedOutput: []string{
				"k8s-*-*-test-0*",
				"k8s-*-*-test-1*",
				"k8s-*-*-test-2*",
				"k8s-*-*-test-3*",
				"k8s-*-*-test-4*",
				"k8s-*-*-test-5*",
				"k8s-*-*-test-6*",
				"k8s-*-*-test-7*",
				"k8s-*-*-test-8*",
				"k8s-*-*-test-9*",
			},
		},
	}

	for _, c := range testCases {
		cli.config = &config.Config{FlagILMFormat: c.ilmFormat}
		actualOutput := cli.GetRolloverIndexPattern(c.labelValue)
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestParseIndexPatternFromIndexTemplate(t *testing.T) {
	testCases := []struct {
		resBody        string
		expectedOutput []string
		version        *semver.Version
	}{
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"mytemp":{
			"order" : 0,
			"version" : 7000199,
			"index_patterns" : [
			  ".monitoring-kibana-7-*"
			],
			"settings" : {
			  "index" : {
				"format" : "7",
				"codec" : "best_compression",
				"number_of_shards" : "1",
				"auto_expand_replicas" : "0-1",
				"number_of_replicas" : "0"
			  }
			}}}`,
			expectedOutput: []string{".monitoring-kibana-7-*"},
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"mytemp":{
			"index_patterns" : [
			  "test-index-esrc*"
			]
			}}`,
			expectedOutput: []string{"test-index-esrc*"},
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"mytemp":{
				"index_patterns" : [
				  "mytest",
				  "bla"
				]
				}}`,
			expectedOutput: []string{"mytest", "bla"},
		},
		{
			version:        semver.MustParse("7.8.2"),
			resBody:        `{"index_templates":[{"name":"helloworld_es-rollover-controller","index_template":{"index_patterns":["helloworld*"],"template":{"settings":{"index":{"lifecycle":{"name":"es-rollover-controller","rollover_alias":"helloworld_writealias"}}}},"composed_of":[],"_meta":{"managed_by":"es-rollover-controller"}}}]}`,
			expectedOutput: []string{"helloworld*"},
		},
	}

	es := ESClient{}

	for _, c := range testCases {
		es.Version = c.version
		actualOutput := es.ParseIndexPatternFromIndexTemplate("mytemp", []byte(c.resBody))
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetILMPolicyNameFromIndexTemplate(t *testing.T) {
	testCases := []struct {
		version        *semver.Version
		resBody        string
		expectedOutput string
	}{
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"test_es-rollover-controller":{
			"settings" : {
			  "index" : {
				"lifecycle" : {
					"name" : "my-custom-ilm-policy",
					"rollover_alias" : "wasd"
				}		  
			  }
			}}}`,
			expectedOutput: "my-custom-ilm-policy",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"test_es-rollover-controller":{
			"order" : 0,
			"version" : 7000199,
			"index_patterns" : [
			  ".monitoring-kibana-7-*"
			],
			"settings" : {
			  "index" : {
				"lifecycle" : {
					"name" : "ilm-history-ilm-policy",
					"rollover_alias" : "ilm-history-1"
				},		  
				"format" : "7",
				"codec" : "best_compression",
				"number_of_shards" : "1",
				"auto_expand_replicas" : "0-1",
				"number_of_replicas" : "0"
			  }
			}}}`,
			expectedOutput: "ilm-history-ilm-policy",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"test_es-rollover-controller":{
			"order" : 0,
			"version" : 7000199,
			"index_patterns" : [
			  ".monitoring-kibana-7-*"
			],
			"settings" : {
			  "index" : {
				"format" : "7",
				"codec" : "best_compression",
				"number_of_shards" : "1",
				"auto_expand_replicas" : "0-1",
				"number_of_replicas" : "0"
			  }
			}}}`,
			expectedOutput: "",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"test_es-rollover-controller":{
			"index_patterns" : [
			  "test-index-esrc*"
			]
			}}`,
			expectedOutput: "",
		},
		{
			version:        semver.MustParse("7.8.2"),
			resBody:        `{"index_templates":[{"name":"helloworld_es-rollover-controller","index_template":{"index_patterns":["helloworld*"],"template":{"settings":{"index":{"lifecycle":{"name":"es-rollover-controller","rollover_alias":"helloworld_writealias"}}}},"composed_of":[],"_meta":{"managed_by":"es-rollover-controller"}}}]}`,
			expectedOutput: "es-rollover-controller",
		},
	}

	es := ESClient{}

	for _, c := range testCases {
		es.Version = c.version
		actualOutput := es.ParseILMPolicyNameFromIndexTemplate("test_es-rollover-controller", []byte(c.resBody))
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestGetRolloverAliasFromIndexTemplate(t *testing.T) {
	testCases := []struct {
		version        *semver.Version
		resBody        string
		expectedOutput string
	}{
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"myIndexTemplate":{
			"settings" : {
			  "index" : {
				"lifecycle" : {
					"name" : "my-custom-ilm-policy",
					"rollover_alias" : "wasd"
				}		  
			  }
			}}}`,
			expectedOutput: "wasd",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"myIndexTemplate":{
			"order" : 0,
			"version" : 7000199,
			"index_patterns" : [
			  ".monitoring-kibana-7-*"
			],
			"settings" : {
			  "index" : {
				"lifecycle" : {
					"name" : "ilm-history-ilm-policy",
					"rollover_alias" : "my-custom-alias"
				},		  
				"format" : "7",
				"codec" : "best_compression",
				"number_of_shards" : "1",
				"auto_expand_replicas" : "0-1",
				"number_of_replicas" : "0"
			  }
			}}}`,
			expectedOutput: "my-custom-alias",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"myIndexTemplate":{
			"order" : 0,
			"version" : 7000199,
			"index_patterns" : [
			  ".monitoring-kibana-7-*"
			],
			"settings" : {
			  "index" : {
				"format" : "7",
				"codec" : "best_compression",
				"number_of_shards" : "1",
				"auto_expand_replicas" : "0-1",
				"number_of_replicas" : "0"
			  }
			}}}`,
			expectedOutput: "",
		},
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"index_patterns" : [
			  "test-index-esrc*"
			]
			}`,
			expectedOutput: "",
		},
		{
			version:        semver.MustParse("7.8.2"),
			resBody:        `{"index_templates":[{"name":"helloworld_es-rollover-controller","index_template":{"index_patterns":["helloworld*"],"template":{"settings":{"index":{"lifecycle":{"name":"es-rollover-controller","rollover_alias":"helloworld_writealias"}}}},"composed_of":[],"_meta":{"managed_by":"es-rollover-controller"}}}]}`,
			expectedOutput: "helloworld_writealias",
		},
	}

	es := ESClient{}

	for _, c := range testCases {
		es.Version = c.version
		actualOutput := es.ParseRolloverAliasFromIndexTemplate("myIndexTemplate", []byte(c.resBody))
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func TestParseManagedIndexTemplates(t *testing.T) {
	testCases := []struct {
		resp           string
		expectedOutput []string
	}{
		{
			resp:           `{"index_templates":[{"name":"wasd"},{"name":"vpa-updater_es-rollover-controller","index_template":{"_meta":{"managed_by":"es-rollover-controller"}}},{"name":"vpa-recommender_es-rollover-controller","index_template":{"_meta":{"managed_by":"es-rollover-controller"}}}]}`,
			expectedOutput: []string{"vpa-updater_es-rollover-controller", "vpa-recommender_es-rollover-controller"},
		},
		{
			resp:           `{"index_templates":[{"name":"yay"},{"name":"fake-updater_es-rollover-controller","index_template":{"_meta":{"managed_by":"another-rollover-controller"}}},{"name":"vpa-recommender_es-rollover-controller","index_template":{"_meta":{"managed_by":"es-rollover-controller"}}}]}`,
			expectedOutput: []string{"vpa-recommender_es-rollover-controller"},
		},
		{
			resp:           `{}`,
			expectedOutput: []string{},
		},
	}

	for _, c := range testCases {
		es := ESClient{config: &config.Config{FlagControllerID: "es-rollover-controller"}}
		actualOutput, err := es.parseManagedIndexTemplates([]byte(c.resp))
		assert.NoError(t, err)
		assert.ElementsMatch(t, c.expectedOutput, actualOutput)
	}
}

func TestGetComponentTemplatesFromIndexTemplate(t *testing.T) {
	testCases := []struct {
		version        *semver.Version
		resBody        string
		expectedOutput *[]string
	}{
		{
			version: semver.MustParse("7.6.2"),
			resBody: `{
			"myIndexTemplate":{
			"settings" : {
			  "index" : {
				"lifecycle" : {
					"name" : "my-custom-ilm-policy",
					"rollover_alias" : "wasd"
				}		  
			  }
			}}}`,
			expectedOutput: nil,
		},
		{
			version: semver.MustParse("7.8.2"),
			resBody: `{
	"index_templates": [
		{
			"name": "helloworld_es-rollover-controller",
			"index_template": {
				"index_patterns": [
					"helloworld*"
				],
				"template": {
					"settings": {
						"index": {
							"lifecycle": {
								"name": "es-rollover-controller",
								"rollover_alias": "helloworld_writealias"
							}
						}
					}
				},
				"composed_of": [],
				"_meta": {
					"managed_by": "es-rollover-controller"
				}
			}
		}
	]
}`,
			expectedOutput: nil,
		},
		{
			version: semver.MustParse("7.8.2"),
			resBody: `{
	"index_templates": [
		{
			"name": "helloworld_es-rollover-controller",
			"index_template": {
				"index_patterns": [
					"helloworld*"
				],
				"template": {
					"settings": {
						"index": {
							"lifecycle": {
								"name": "es-rollover-controller",
								"rollover_alias": "helloworld_writealias"
							}
						}
					}
				},
				"composed_of": ["test","yay"],
				"_meta": {
					"managed_by": "es-rollover-controller"
				}
			}
		}
	]
}`,
			expectedOutput: &[]string{"test", "yay"},
		},
	}

	es := ESClient{}

	for _, c := range testCases {
		es.Version = c.version
		actualOutput := es.ParseComponentTemplatesFromIndexTemplate("myIndexTemplate", []byte(c.resBody))
		assert.Equal(t, c.expectedOutput, actualOutput)
	}
}

func Test_stringSliceEqual(t *testing.T) {
	type args struct {
		a []string
		b []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "equal_orderd", args: args{a: []string{"a", "b"}, b: []string{"a", "b"}}, want: true},
		{name: "not_equal_unorderd", args: args{a: []string{"b", "a"}, b: []string{"a", "b"}}, want: false},
		{name: "not_equal_a_less_elements", args: args{a: []string{"b"}, b: []string{"b", "a"}}, want: false},
		{name: "not_equal_b_less_elements", args: args{a: []string{"a", "b"}, b: []string{"a"}}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := stringSliceEqual(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("stringSliceEqual() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_pointerStringSliceEqual(t *testing.T) {
	type args struct {
		a *[]string
		b *[]string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "a_nil", args: args{a: nil, b: &[]string{"a", "b"}}, want: false},
		{name: "b_nil", args: args{a: &[]string{"a", "b"}, b: nil}, want: false},
		{name: "both_nil", args: args{a: nil, b: nil}, want: true},
		{name: "equal_orderd", args: args{a: &[]string{"a", "b"}, b: &[]string{"a", "b"}}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pointerStringSliceEqual(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("pointerStringSliceEqual() = %v, want %v", got, tt.want)
			}
		})
	}
}
