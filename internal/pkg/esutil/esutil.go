package esutil

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/Masterminds/semver"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/valyala/fastjson"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/instrumentation"

	elasticsearch "github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
)

const (
	defaultILMPolicy = `{"policy": { "phases": { "hot": { "actions": { "rollover": { "max_age": "30d", "max_size": "50gb" }}}}}}`
	// ActionCreateIndexTemplate identifies the create index template action
	ActionCreateIndexTemplate = "CreateIndexTemplate"
	// ActionCreateRolloverIndexAlias identifies the create rollover index alias action
	ActionCreateRolloverIndex = "CreateRolloverIndexAlias"
	// ActionCreateILMPolicy identifies the create ILM policy action
	ActionCreateILMPolicy = "CreateILMPolicy"
)

// ESClient stores the elasticsearch client and additional configuration
type ESClient struct {
	client           *elasticsearch.Client
	config           *config.Config
	Version          *semver.Version
	PerformedActions []string
	MetricsCollector *instrumentation.MetricsCollector
}

// NewESClient creates a new ESClient object
func NewESClient(config *config.Config, metricsCollector *instrumentation.MetricsCollector) (*ESClient, error) {
	caCertPool, err := x509.SystemCertPool()
	if err != nil {
		log.Fatal(err)
	}

	if config.FlagESCACertFile != "" {
		caCert, err := ioutil.ReadFile(config.FlagESCACertFile)
		if err != nil {
			log.Fatal(err)
		}
		caCertPool.AppendCertsFromPEM(caCert)
	}

	esTimeout, _ := time.ParseDuration(config.FlagESTimeout)

	cfg := elasticsearch.Config{
		Addresses: []string{
			fmt.Sprintf("%s:%s", config.FlagESHost, config.FlagESPort),
		},
		Username: config.FlagESUsername,
		Password: config.FlagESPassword,
		Transport: &http.Transport{
			MaxIdleConnsPerHost:   10,
			ResponseHeaderTimeout: esTimeout,
			DialContext:           (&net.Dialer{Timeout: esTimeout}).DialContext,
			TLSClientConfig: &tls.Config{
				MinVersion:         tls.VersionTLS11,
				RootCAs:            caCertPool,
				InsecureSkipVerify: false,
				// VerifyPeerCertificate: func(rawCerts [][]byte, verifiedChains [][]*x509.Certificate) error {
				// 	return fmt.Errorf("%s // %s", rawCerts, verifiedChains)
				// },
			},
		},
	}

	client, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	_, err = client.Ping()
	if err != nil {
		return nil, err
	}

	esClient := ESClient{
		client:           client,
		config:           config,
		MetricsCollector: metricsCollector,
	}

	// retrieve version of es cluster
	v, err := esClient.getClusterVersion()
	if err != nil {
		return nil, err
	}
	esClient.Version = v

	return &esClient, nil
}

// getClusterVersion returns the version of the elasticsearch cluster
func (es *ESClient) getClusterVersion() (*semver.Version, error) {
	res, err := es.client.Info()
	defer res.Body.Close()

	if err != nil {
		return nil, errors.Wrap(err, "retrieving cluster version")
	}

	// check response status
	if res.IsError() {
		return nil, errors.New(res.String())
	}

	// deserialize the response into a map.
	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, errors.Wrap(err, "Error parsing the response body")
	}

	// parse the semantic version
	rawVersion := fmt.Sprintf("%s", r["version"].(map[string]interface{})["number"])
	v, err := semver.NewVersion(rawVersion)
	if err != nil {
		return nil, errors.Wrap(err, "parsing SemVer")
	}

	return v, nil
}

// AliasExists checks whether an alias has been created already
func (es *ESClient) AliasExists(labelValue string) (bool, error) {
	res, err := es.client.Cat.Aliases(
		es.client.Cat.Aliases.WithName(es.GetWriteAlias(labelValue)),
		es.client.Cat.Aliases.WithFormat("json"),
	)
	defer res.Body.Close()

	if err != nil {
		return false, err
	}

	if res.IsError() {
		return false, fmt.Errorf("elasticsearch error: %s", res.String())
	}

	var jsonRes []map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&jsonRes); err != nil {
		return false, fmt.Errorf("error decoding CatAliases json response: %s", err)
	}

	for _, a := range jsonRes {
		if a["alias"] == es.GetWriteAlias(labelValue) {
			return true, nil
		}
	}
	return false, nil
}

// CreateRolloverIndex creates the rollover index and its write alias
func (es *ESClient) CreateRolloverIndex(elasticIndexLabelValue string, customILMPolicyLabelValue string) error {
	rolloverIndex := es.generateRolloverIndex(elasticIndexLabelValue, customILMPolicyLabelValue)

	indexBody, err := json.Marshal(rolloverIndex)
	if err != nil {
		return err
	}

	res, err := es.client.Indices.Create(
		es.GetRolloverIndex(elasticIndexLabelValue),
		es.client.Indices.Create.WithBody(strings.NewReader(fmt.Sprintf("%s", indexBody))),
	)
	if err != nil {
		return err
	}

	if res.IsError() {
		return fmt.Errorf("elasticsearch error: %s", res.String())
	}

	es.PerformedActions = append(es.PerformedActions, ActionCreateRolloverIndex)
	es.MetricsCollector.ObservePerformedAction(ActionCreateRolloverIndex)

	return nil
}

// RequestIndexTemplate requests an index template from elasticsearch and returns the body
func (es *ESClient) RequestIndexTemplate(indexTemplateName string) ([]byte, *esapi.Response, error) {
	var res *esapi.Response
	var err error

	if es.HasNewIndexTemplatesSupport() {
		res, err = es.client.Indices.GetIndexTemplate(es.client.Indices.GetIndexTemplate.WithName(indexTemplateName))
	} else {
		res, err = es.client.Indices.GetTemplate(es.client.Indices.GetTemplate.WithName(indexTemplateName))
	}
	defer res.Body.Close()

	if err != nil {
		return nil, res, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, nil, fmt.Errorf("error reading response body: %s", res.String())
	}
	return body, res, err
}

// FindManagedIndexTemplates retrieves all index templates that are managed by the controller
func (es *ESClient) FindManagedIndexTemplates() ([]string, error) {
	res, err := es.client.Indices.GetIndexTemplate(
		es.client.Indices.GetIndexTemplate.WithFilterPath(
			"index_templates.name,index_templates.index_template._meta.managed_by",
		),
	)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	if err != nil {
		return nil, fmt.Errorf("error reading response body: %s", res.String())
	}

	return es.parseManagedIndexTemplates(body)
}

// DeleteIndexTemplate removes an index template from elasticsearch
func (es *ESClient) DeleteIndexTemplate(name string) error {
	res, err := es.client.Indices.DeleteIndexTemplate(name, es.client.Indices.DeleteIndexTemplate.WithFilterPath(""))
	defer res.Body.Close()

	if err != nil {
		return err
	}

	if res.IsError() {
		return errors.New(res.String())
	}

	return nil
}

// parseManagedIndexTemplates parses the list index tempates v2 api response to a slice of strings containing all managed index templates
func (es *ESClient) parseManagedIndexTemplates(res []byte) ([]string, error) {
	type templateData struct {
		Name          string                       `json:"name"`
		IndexTemplate map[string]map[string]string `json:"index_template"`
	}

	var templateResp map[string][]templateData
	err := json.Unmarshal(res, &templateResp)
	if err != nil {
		return []string{}, err
	}

	managedIndexTemplates := []string{}

	templates, ok := templateResp["index_templates"]
	if !ok {
		return managedIndexTemplates, nil
	}

	for _, t := range templates {
		meta, ok := t.IndexTemplate["_meta"]
		if !ok {
			continue
		}

		managedBy, ok := meta["managed_by"]
		if !ok {
			continue
		}

		if managedBy != es.config.FlagControllerID {
			continue
		}

		managedIndexTemplates = append(managedIndexTemplates, t.Name)

	}
	return managedIndexTemplates, nil
}

// IndexTemplateUpToDate checks if the index template matches the defined state
func (es *ESClient) IndexTemplateUpToDate(
	elasticIndexlabelValue string,
	customILMPolicyLabelValue string,
	componentTemplatesValue string,
) (bool, error) {
	indexTemplateName := es.GetIndexTemplateName(elasticIndexlabelValue)

	body, res, err := es.RequestIndexTemplate(indexTemplateName)
	if err != nil {
		return false, err
	}

	if res.StatusCode == 404 {
		return false, nil
	}

	if res.IsError() {
		return false, fmt.Errorf("elasticsearch error: %s", res.String())
	}

	// check whether the current index template matches the defined state
	currentILMPolicy := es.ParseILMPolicyNameFromIndexTemplate(indexTemplateName, body)
	expectedILMPolicy := es.GetILMPolicyName(customILMPolicyLabelValue)
	ilmPolicyOK := currentILMPolicy == expectedILMPolicy

	currentRolloverAlias := es.ParseRolloverAliasFromIndexTemplate(indexTemplateName, body)
	expectedRolloverAlias := es.GetWriteAlias(elasticIndexlabelValue)
	rolloverAliasOK := currentRolloverAlias == expectedRolloverAlias

	currentIndexPatterns := es.ParseIndexPatternFromIndexTemplate(indexTemplateName, body)
	expectedIndexPatterns := es.GetRolloverIndexPattern(elasticIndexlabelValue)
	indexPatternOK := stringSliceEqual(currentIndexPatterns, expectedIndexPatterns)

	currentComponentTemplates := es.ParseComponentTemplatesFromIndexTemplate(indexTemplateName, body)
	expectedComponentTemplates := es.GetComponentTemplates(componentTemplatesValue)
	componentTemplatesOK := pointerStringSliceEqual(currentComponentTemplates, expectedComponentTemplates)

	return ilmPolicyOK && rolloverAliasOK && indexPatternOK && componentTemplatesOK, nil
}

// EnsureIndexTemplate ensures the index template matches the defined state, including the rollover alias and ILM policy
func (es *ESClient) EnsureIndexTemplate(
	elasticIndexLabelValue string,
	customILMPolicyLabelValue string,
	componentTemplatesValue string,
) error {
	templateBody, err := es.generateIndexTemplateBody(
		elasticIndexLabelValue,
		customILMPolicyLabelValue,
		componentTemplatesValue,
	)
	if err != nil {
		return err
	}

	templateBodyJson, err := json.Marshal(templateBody)
	if err != nil {
		return err
	}

	bodyReader := strings.NewReader(fmt.Sprintf("%s", templateBodyJson))

	var res *esapi.Response

	if es.HasNewIndexTemplatesSupport() {
		// use the new /_index_template api including metadata
		res, err = es.client.Indices.PutIndexTemplate(es.GetIndexTemplateName(elasticIndexLabelValue), bodyReader)
	} else {
		// fallback to the old /_template api without metadata
		res, err = es.client.Indices.PutTemplate(es.GetIndexTemplateName(elasticIndexLabelValue), bodyReader)
	}
	if err != nil {
		return err
	}

	if res.IsError() {
		return fmt.Errorf("elasticsearch error: %s", res.String())
	}

	es.PerformedActions = append(es.PerformedActions, ActionCreateIndexTemplate)
	es.MetricsCollector.ObservePerformedAction(ActionCreateIndexTemplate)

	return nil
}

// ilmPolicyExists checks if the ILM policy has already been created
func (es *ESClient) ilmPolicyExists() (bool, error) {
	res, err := es.client.ILM.GetLifecycle(es.client.ILM.GetLifecycle.WithPolicy(es.config.FlagILMPolicyName))

	if res != nil {
		defer res.Body.Close()
	}

	if err != nil {
		return false, err
	}

	if res.StatusCode == 404 {
		return false, nil
	}

	if res.IsError() {
		return false, fmt.Errorf("elasticsearch error: %s", res.String())
	}

	var jsonRes map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&jsonRes); err != nil {
		return false, fmt.Errorf("error decoding GetLifecycle json response: %s", err)
	}

	// TODO: check if policy matches the desired state
	return true, nil
}

// createILMPolicy creates the ILM policy managed by the controller
func (es *ESClient) createILMPolicy() error {
	var policyReader io.Reader

	if es.config.FlagILMPolicyFile == "" {
		policyReader = strings.NewReader(defaultILMPolicy)
	} else {
		var err error
		policyReader, err = os.Open(es.config.FlagILMPolicyFile)

		if err != nil {
			return errors.Wrap(err, "reading ILM policy file")
		}
	}

	res, err := es.client.ILM.PutLifecycle(
		es.config.FlagILMPolicyName,
		es.client.ILM.PutLifecycle.WithBody(policyReader),
	)
	if err != nil {
		return err
	}

	if res.IsError() {
		return fmt.Errorf("elasticsearch error: %s", res.String())
	}

	es.PerformedActions = append(es.PerformedActions, ActionCreateILMPolicy)
	es.MetricsCollector.ObservePerformedAction(ActionCreateILMPolicy)

	return nil
}

// Bootstrap runs the initial bootstrapping against elasticsearch during startup
func (es *ESClient) Bootstrap() error {
	// TODO: add logging
	return es.ensureILMPolicyExists()
}

// ensureILMPolicyExists ensures that the ILM policy exists and overwrites it when specified
func (es *ESClient) ensureILMPolicyExists() error {
	exists, err := es.ilmPolicyExists()
	if err != nil {
		return errors.Wrap(err, "checking ILM policy exists")
	}

	if !exists || es.config.FlagILMPolicyOverwrite {
		log.Infof("Writing ILM Policy %s", es.config.FlagILMPolicyName)
		return es.createILMPolicy()
	}

	return nil
}

// GetWriteAlias returns the write alias
func (es *ESClient) GetWriteAlias(labelValue string) string {
	return fmt.Sprintf(es.config.FlagWriteAliasFormat, labelValue)
}

// GetIndexTemplateName returns the index template name
func (es *ESClient) GetIndexTemplateName(labelValue string) string {
	return fmt.Sprintf(es.config.FlagIndexTemplateFormat, labelValue)
}

// GetRolloverIndex returns the name of the initial index which will be rolled-over continuously
func (es *ESClient) GetRolloverIndex(labelValue string) string {
	return urlEscapeString(fmt.Sprintf(es.config.FlagILMFormat, labelValue))
}

// baseIndexPatternPrefix returns the prefix of an index pattern like my-rollover-index
func (es *ESClient) baseIndexPatternPrefixFromILMFormat(labelValue string) string {
	// remove surrounding angle brackets if present in date math index patterns
	pattern := strings.Trim(es.config.FlagILMFormat, "<>")

	// replace all dateMath expressions with *
	re := regexp.MustCompile(`[^\\]({.+?})`)
	dateMathExpressions := re.FindAllStringSubmatch(pattern, -1)
	for _, m := range dateMathExpressions {
		if len(m) > 1 {
			for _, dm := range m[1:] {
				pattern = strings.Replace(pattern, dm, "*", 1)
			}
		}
	}

	// replace %s with the label value
	pattern = fmt.Sprintf(pattern, labelValue)

	// remove -000001 suffix
	return strings.TrimSuffix(pattern, config.ILMIncrementPattern)
}

// GetKibanaIndexPattern returns the index pattern used to configure in Kibana
func (es *ESClient) GetKibanaIndexPattern(labelValue string) string {
	return fmt.Sprintf("%s-*", es.baseIndexPatternPrefixFromILMFormat(labelValue))
}

// GetRolloverIndexPattern returns the index pattern of the rollover index
func (es *ESClient) GetRolloverIndexPattern(labelValue string) []string {
	pattern := es.baseIndexPatternPrefixFromILMFormat(labelValue)

	// create a list of patterns by appending -[0-1]* to the pattern, to avoid matching false indices (see: https://gitlab.com/msvechla/es-rollover-controller/-/issues/29)
	finalPatterns := []string{}
	for _, n := range []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9} {
		finalPatterns = append(finalPatterns, urlEscapeString(fmt.Sprintf("%s-%d*", pattern, n)))
	}

	return finalPatterns
}

// GetILMPolicyName returns the name of the ilm policy to attach, based on the value of the ilm policy label
func (es *ESClient) GetILMPolicyName(labelValue string) string {
	if labelValue == "" {
		return es.config.FlagILMPolicyName
	}
	return labelValue
}

// ParseIndexPatternFromIndexTemplate returns the index patterns from a get index template response body. Returns an empty string if none is defined.
func (es *ESClient) ParseIndexPatternFromIndexTemplate(indexTemplateName string, resBody []byte) []string {
	indexTemplates := []string{}
	var path []string

	if es.HasNewIndexTemplatesSupport() {
		path = []string{"index_templates", "0", "index_template", "index_patterns"}
	} else {
		path = []string{indexTemplateName, "index_patterns"}
	}

	var p fastjson.Parser
	v, err := p.Parse(string(resBody))
	if err != nil {
		return indexTemplates
	}

	values := v.GetArray(path...)
	for _, val := range values {
		stringVal, err := val.StringBytes()
		if err != nil {
			continue
		}

		indexTemplates = append(indexTemplates, string(stringVal))
	}

	return indexTemplates
}

// ParseILMPolicyNameFromIndexTemplate returns the ILM policy name from a get index template response body. Returns an empty string if none is defined.
func (es *ESClient) ParseILMPolicyNameFromIndexTemplate(indexTemplateName string, resBody []byte) string {
	var path []string
	if es.HasNewIndexTemplatesSupport() {
		path = []string{"index_templates", "0", "index_template", "template", "settings", "index", "lifecycle", "name"}
	} else {
		path = []string{indexTemplateName, "settings", "index", "lifecycle", "name"}
	}
	return fastjson.GetString(resBody, path...)
}

// ParseRolloverAliasFromIndexTemplate returns the rollover alias from a get index template response body. Returns an empty string if none is defined.
func (es *ESClient) ParseRolloverAliasFromIndexTemplate(indexTemplateName string, resBody []byte) string {
	var path []string
	if es.HasNewIndexTemplatesSupport() {
		path = []string{
			"index_templates",
			"0",
			"index_template",
			"template",
			"settings",
			"index",
			"lifecycle",
			"rollover_alias",
		}
	} else {
		path = []string{indexTemplateName, "settings", "index", "lifecycle", "rollover_alias"}
	}
	return fastjson.GetString(resBody, path...)
}

// ParseComponentTemplatesFromIndexTemplate returns the comopnent templates from a get index template response body. Returns an empty string if none is defined.
func (es *ESClient) ParseComponentTemplatesFromIndexTemplate(indexTemplateName string, resBody []byte) *[]string {
	if !es.HasNewIndexTemplatesSupport() {
		return nil
	}

	componentTemplates := []string{}
	path := []string{"index_templates", "0", "index_template", "composed_of"}

	var p fastjson.Parser
	v, err := p.Parse(string(resBody))
	if err != nil {
		return nil
	}

	values := v.GetArray(path...)
	for _, val := range values {
		stringVal, err := val.StringBytes()
		if err != nil {
			continue
		}

		componentTemplates = append(componentTemplates, string(stringVal))
	}

	if len(componentTemplates) == 0 {
		return nil
	}
	return &componentTemplates
}

// GetNamespaceScope returns the namespace scope
func (es *ESClient) GetNamespaceScope() string {
	return es.config.FlagNamespaceScope
}

// GetClient returns the underlying elasticsearch client, should mainly be used for tests
func (es *ESClient) GetClient() *elasticsearch.Client {
	return es.client
}

func urlEscapeString(s string) string {
	escaped := url.PathEscape(s)

	// do not escape * to stay backwards compatible for most cases
	return strings.Replace(escaped, "%2A", "*", -1)
}

func stringSliceEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func pointerStringSliceEqual(a, b *[]string) bool {
	if a == b {
		return true
	}
	if a == nil && b != nil {
		return false
	}
	if b == nil && a != nil {
		return false
	}
	return stringSliceEqual(*a, *b)
}
