## [0.12.0](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.5...0.12.0) (2023-12-13)


### Bug Fixes

* semantic-release ([eb128b1](https://gitlab.com/msvechla/es-rollover-controller/commit/eb128b1934c59d247471f5e1c90c7cb9cfb5279f))
* update to go v1.21 ([6013e3d](https://gitlab.com/msvechla/es-rollover-controller/commit/6013e3d9571ccfb002e40f9caeb07aa7ff256955))


### Features

* update vpa to v1 ([f9b6ec8](https://gitlab.com/msvechla/es-rollover-controller/commit/f9b6ec8235c42a6ee70b9b95e277774401190897))

## [0.11.5](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.4...0.11.5) (2023-04-19)


### Bug Fixes

* **kibana:** use correct api to ensure data views are up to date ([7ca873e](https://gitlab.com/msvechla/es-rollover-controller/commit/7ca873e1609174cb1e8c35fb2a3660c81ab44463))

## [0.11.4](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.3...0.11.4) (2023-04-18)


### Bug Fixes

* **kibana:** fix dataview reconcile logic ([920a52f](https://gitlab.com/msvechla/es-rollover-controller/commit/920a52fce2e2e3e88b858e38ff25e0cfa0661e19))
* **kibana:** remove references to index patterns in logs ([839e632](https://gitlab.com/msvechla/es-rollover-controller/commit/839e63255ded9b9fdee8a027c26201764f85029e))

## [0.11.3](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.2...0.11.3) (2023-04-18)


### Bug Fixes

* **kibana:** fix data view timestamp field and keep data view in sync ([c4a3291](https://gitlab.com/msvechla/es-rollover-controller/commit/c4a3291324fbb0b224cdf521d4ea9b5b75586cb3))

## [0.11.2](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.1...0.11.2) (2023-03-31)


### Bug Fixes

* **kibana:** actually use http client with cert pool ([40c5c26](https://gitlab.com/msvechla/es-rollover-controller/commit/40c5c26c6b5f8e14baf66a77a645d8f469fb61ac))

## [0.11.1](https://gitlab.com/msvechla/es-rollover-controller/compare/0.11.0...0.11.1) (2023-03-30)


### Bug Fixes

* **semantic-release:** helm-docs binary ([4edb308](https://gitlab.com/msvechla/es-rollover-controller/commit/4edb3083019dfe263a951928e52c18c3cc9339ce))

## [0.11.0](https://gitlab.com/msvechla/es-rollover-controller/compare/0.10.0...0.11.0) (2023-03-30)


### Features

* **helm:** add kibana ca cert support ([3d13752](https://gitlab.com/msvechla/es-rollover-controller/commit/3d137528b5bfe3603f64cd54de8dc4075ab7f72c))
* **kibana:** add support for kibana ca cert file ([57c054e](https://gitlab.com/msvechla/es-rollover-controller/commit/57c054e648a4fc6ba39e9ea376f80556f93d6f0d))

## [0.10.0](https://gitlab.com/msvechla/es-rollover-controller/compare/0.9.0...0.10.0) (2023-03-23)


### Features

* **#41:** use data views instead of index patterns ([28420dc](https://gitlab.com/msvechla/es-rollover-controller/commit/28420dce272caa678f3a60e1b84e598468b715d8)), closes [#41](https://gitlab.com/msvechla/es-rollover-controller/issues/41)
* **container:** switch to distroless image ([032e845](https://gitlab.com/msvechla/es-rollover-controller/commit/032e8451921cb902cf375ac484da42007e720716))
* **helm:** add data view support to helm chart ([94329d6](https://gitlab.com/msvechla/es-rollover-controller/commit/94329d63d75ce3c24d37b255601456aee603e0b5))


### Bug Fixes

* **go:** update go modules ([f947988](https://gitlab.com/msvechla/es-rollover-controller/commit/f947988f7fb040f454f80c6cfffd6c7b67d89644))

## [0.9.0](https://gitlab.com/msvechla/es-rollover-controller/compare/0.8.3...0.9.0) (2023-02-06)


### Features

* **helm:** add KIBANA_URL and CREATE_INDEX_PATTERNS config ([1c01b92](https://gitlab.com/msvechla/es-rollover-controller/commit/1c01b925d6de166330749ce64e2e9cb423f0b932))
* **kibana:** index pattern support via Kibana API ([94a4bed](https://gitlab.com/msvechla/es-rollover-controller/commit/94a4bedf6e269d225be23560f6e06fa73cd39ca3)), closes [#40](https://gitlab.com/msvechla/es-rollover-controller/issues/40)


### Bug Fixes

* **deps:** update dependencies ([59cc14e](https://gitlab.com/msvechla/es-rollover-controller/commit/59cc14e47e5fb517989825a0d66499ff61a9eaeb))

## [0.8.3](https://gitlab.com/msvechla/es-rollover-controller/compare/0.8.2...0.8.3) (2022-09-22)


### Bug Fixes

* **docker:** fix broken arm64 docker builds, fixes [#35](https://gitlab.com/msvechla/es-rollover-controller/issues/35) ([f9f0ff4](https://gitlab.com/msvechla/es-rollover-controller/commit/f9f0ff4981ed3a2948d389d3b56b4ac3d0fd7370))

## [0.8.2](https://gitlab.com/msvechla/es-rollover-controller/compare/0.8.1...0.8.2) (2022-09-19)


### Bug Fixes

* **deps:** update module dependencies ([46dccc8](https://gitlab.com/msvechla/es-rollover-controller/commit/46dccc8d274d7a34556b1c1562eecffccd1ced42))
* **deps:** update to go 1.19 ([0ec960f](https://gitlab.com/msvechla/es-rollover-controller/commit/0ec960ff5a794fbc2e88f5c360c35c429765de0a))

### [0.8.1](https://gitlab.com/msvechla/es-rollover-controller/compare/0.8.0...0.8.1) (2022-02-02)


### Bug Fixes

* **renovate:** update module github.com/elastic/go-elasticsearch/v7 to v7.17.0 ([17f5746](https://gitlab.com/msvechla/es-rollover-controller/commit/17f5746457b9ec56e9827b1a6ad0f2898675ef52))

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.8.0] - 2022-02-01

### Added

- releases via goreleaser
- multi-arch docker builds
- binary releases for windows, linux and macos

### Changed

- updated several dependencies
- updated docker base image

## [0.7.0] - 2021-09-16

### Added

- `COMPONENT_TEMPLATES_ANNOTATION_KEY` option to specify a pod annotation key, that can contain a list of component templates that will get merged into the default index template ([see !27](https://gitlab.com/msvechla/es-rollover-controller/-/issues/27))
- helm support for COMPONENT_TEMPLATES_ANNOTATION_KEY
- updated container image to latest alpine

## [0.6.0] - 2021-08-04

### Added

- `EXPOSE_PROMETHEUS_METRICS` option to expose controller metrics in the Prometheus format
- controller metrics for cache statistics and operations performed
- updated Kubernetes clients to `v0.21.3`
- updated container image to latest alpine
- add option to enable `PodMonitor` creation to the helm chart

## [0.5.1] - 2021-06-16

### Fixed

- use `GARBAGE_COLLECTION_INTERVAL` to configure the garbage collection interval instead of `CACHE_TTL` (MR by @penekk 👏 / [see !47](https://gitlab.com/msvechla/es-rollover-controller/-/merge_requests/47))

### Added

- `CLEANUP_LEGACY_INDEX_TEMPLATES`, `GARBAGE_COLLECTION_INTERVAL` and `CONTROLLER_ID` as helm configuration values (MR by @penekk 👏 / [see !47](https://gitlab.com/msvechla/es-rollover-controller/-/merge_requests/47))
- updated elasticsearch client to `v7.13.1.`
- updated go version to `1.16`

## [0.5.0] - 2021-02-14

### Added

- fix a bug where index patterns could match multiple indices ([see #29](https://gitlab.com/msvechla/es-rollover-controller/-/issues/29)). This will adapt all index template patterns by creating an array of matching patterns by appending the following regex to the existing pattern: `[0-9]*`. This will not affect the current behaviour, as indices anyways have to end with the increment pattern suffix
- add `rollover_index_annotation_key` support to helm chart
- fix wrong elastic API usage for cleanup of legacy indices

## [0.4.0] - 2021-02-01

### Added

- support for specifying rollover indices via annotations ([see #28](https://gitlab.com/msvechla/es-rollover-controller/-/issues/28))

### Changed

- update module ReneKroon/ttlcache to v2
- update module elastic/go-elasticsearch/v7 to v7.10.0
- update docker image to latest alpine and go

## [0.3.0] - 2021-01-11

### Added

- support for date math syntax in ILM Format ([see #25](https://gitlab.com/msvechla/es-rollover-controller/-/issues/25))

## [0.2.2] - 2020-12-23

### Fixed

- sensitive config options are now redacted in the logs ([see #26](https://gitlab.com/msvechla/es-rollover-controller/-/issues/26))
- upgrade to latest alpine base image

## [0.2.1] - 2020-09-29

### Fixed

- ttl cache is now invalidated based on ILM policy as well ([see #24](https://gitlab.com/msvechla/es-rollover-controller/-/issues/24))

## [0.2.0] - 2020-08-30

### Added 

- new config option `CONTROLLER_ID` to specify identity of the controller
- support for new `_index_template` v2 API
- v2 index templates now include metadata with controller ownership info
- config option `CLEANUP_LEGACY_INDEX_TEMPLATES` to remove outdated index templates when running against an elasticsearch cluster with v2 support
- support for garbage collection of stale rollover controller artefacts, which cleans up index templates if they are no longer managed by the controller
- new flag `GARBAGE_COLLECTION_INTERVAL` to control the interval at which the garbage collection should run
- running tests against multiple elasticsearch versions

### Changed

- upgraded elasticsearch client to v7.8.0

## [0.1.1] - 2020-06-25

### Fixed

- use system cert pool as base certificate pool as until now an empty pool was used, which could cause issues with trusted certificates (PR by @hieu.le6 👏 / see [!9](https://gitlab.com/msvechla/es-rollover-controller/-/merge_requests/9))

## [0.1.0] - 2020-05-11

### Added

- new option `ILM_POLICY_LABEL_KEY`, which allows attaching custom ILM policies to the index template of specific rollover indices based on a pod label ([see #20](https://gitlab.com/msvechla/es-rollover-controller/-/issues/20))
- updated Helm chart with new option
- improved end to end tests

### Fixed

- state of index templates controlled by the controller is now ensured with improved logic. Before it was only verified if the template exists, now the various settings are ensured as well

## [0.0.9] - 2020-05-02

### Fixed

- update go to 1.14
- update client-go to 1.17.5
- updated all go dependencies to get the latest bug and vulnerability fixes
- update helm and snyk-cli to latest version
- bumped elasticsearch e2e test docker image to 7.6.2

## [0.0.8] - 2019-12-28

### Fixed

- updated all go dependencies to get the latest bug and vulnerability fixes

## [0.0.8] - 2019-12-28

### Fixed

- updated all go dependencies to get the latest bug and vulnerability fixes

## [0.0.7] - 2019-12-17

### Added

- option `elasticsearchTimeout` to specify elasticsearch dialer and response header timeout
- option `workers` to specify the number of worker threads to process kubernetes events
- updated helm chart with new options accordingly

## [0.0.6] - 2019-12-16

### Fixed

- fix an issue where a non-matching index pattern was configured when a custom ILMFormat has been set

## [0.0.5] - 2019-12-12

### Fixed

- fix nilpointer during es bootstrap when elasticsearch is unavailable ([see #6](https://gitlab.com/msvechla/es-rollover-controller/issues/6))

### Added

- implement new TTL Cache for periodically re-ensuring rollover setup for discovered indices ([see #7](https://gitlab.com/msvechla/es-rollover-controller/issues/7))
- added new option `cacheTTL` to helm chart

## [0.0.4] - 2019-10-20

### Breaking Changes

- renamed `ilmPattern` to `ilmFormat` to allow more fined-grained control, default value is unchanged
- renamed `indexTemplateSuffix` to `indexTemplateFormat` to allow more fined-grained control, default value is unchanged
- renamed `writeAliasSuffix` to `writeAliasFormat` to allow more fined-grained control, default value is unchanged

### Added

- service account with least privileges for the controller
- `--namespace_scope` option to specify the scope of the controller (namespaced or cluster-wide)

### Fixed

- setting the `--rollover_index_label_key` option had no effect in past releases

## [0.0.3] - 2019-10-15

### Added

- allow configuration of log format (json or text)
- helm lint step for CI

### Fixed

- adapted CI to push docker images before releasing helm chart

## [0.0.2] - 2019-10-14

### Added

- allow configuration of custom `ilmPattern`

## [0.0.1] - 2019-10-13

### Added

- Initial pre-release of the controller
- Initial release of the matching helm chart
- Initial docker image release
- Quickstart Guide
- Gitlab Page hosting the Helm Repository and a static Readme
- Lots more 🚀
