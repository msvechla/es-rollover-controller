# es-rollover-controller

[![pipeline status](https://gitlab.com/msvechla/es-rollover-controller/badges/master/pipeline.svg)](https://gitlab.com/msvechla/es-rollover-controller/commits/master) [![coverage report](https://gitlab.com/msvechla/es-rollover-controller/badges/master/coverage.svg)](https://gitlab.com/msvechla/es-rollover-controller/commits/master) [![go report](https://goreportcard.com/badge/gitlab.com/msvechla/es-rollover-controller)](https://goreportcard.com/report/gitlab.com/msvechla/es-rollover-controller) [![Docker Pulls](https://badgen.net/docker/pulls/msvechla/es-rollover-controller)](https://hub.docker.com/r/msvechla/es-rollover-controller)

*A Kubernetes Controller for dynamically creating Elasticsearch Write Aliases, Rollover Indices and Index Lifecycle Management Policies based on Pod Labels.*

[[_TOC_]]

## Overview

The goal of the es-rollover-controller is to automate the [Elasticsearch rollover pattern](https://www.elastic.co/blog/managing-time-based-indices-efficiently) (better known as Index Lifecycle Management / ILM) with variable / dynamic index names. The controller is especially designed for cloud-native applications running inside a Kubernetes cluster, logging to Elasticsearch.

Currently there are various limitations with Logstash and Filebeat, which prevent dynamic Index Lifecycle Management, see:

- [https://github.com/logstash-plugins/logstash-output-elasticsearch/issues/858](https://github.com/logstash-plugins/logstash-output-elasticsearch/issues/858)
- [https://discuss.elastic.co/t/index-lifecycle-management-dynamic-rollover-alias-and-template-name/169614](https://discuss.elastic.co/t/index-lifecycle-management-dynamic-rollover-alias-and-template-name/169614)

This controller however is able to completely automate the setup.

This is achieved by watching Kubernetes pods with a specific label, which determines the rollover alias and all necessary resources that should be created to automate the index lifecycle management for logs produced by this pod. To get a better overview of which resources are managed by the controller, check out [Resources Managed by the Controller](#resources-managed-by-the-controller).

The es-rollover-controller should be deployed alongside Filebeat and optionally the [elasticsearch-operator](https://github.com/elastic/cloud-on-k8s) to fully automate logshipping inside a Kubernetes cluster at scale.

For a quick illustration on how the controller works in a bigger context, take a look at the [example scenario](#example-scenario).

More information can also be found in the Medium post ["Efficiently Managing Kubernetes Logs in Elasticsearch at Scale"](https://medium.com/@msvechla/efficiently-managing-kubernetes-logs-in-elasticsearch-at-scale-ff8129859824).

## Getting Started

> WARNING: This controller is currently still under heavy development and should be tested in sandbox environments first.

```sh
helm repo add es-rollover-controller https://msvechla.gitlab.io/es-rollover-controller/
helm install mycontroller es-rollover-controller/es-rollover-controller
```

For a complete introduction on how to setup the Elasticsearch Operator, Filebeat, and es-rollover-controller in a local Minikube cluster, take a look at the [Quickstart Guide](deploy/examples/quickstart/quickstart-guide.md). For a quick deployment of the controller, please check out the provided [Helm chart](https://msvechla.gitlab.io/es-rollover-controller/).

### Deploying the Controller

The latest release can be found at [Docker Hub](https://hub.docker.com/r/msvechla/es-rollover-controller), where all recent releases are provided as Alpine based Docker images. Alternatively feel free to build the latest version from source.

For an example where the controller connects to a [quickstart elasticsearch operator](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html#k8s-deploy-elasticsearch) instance in the same namespace of a Kubernetes cluster, take a look at our [Quickstart Guide](deploy/examples/quickstart/quickstart-guide.md). Of course it can be configured to connect to an external Elasticsearch cluster as well. For all possible configuration options see [Configuration Options](#configuration-options) below.

#### All-In-One Kubernetes Manifests

The very basic configuration is available as [deploy/default-all-in-one.yaml](deploy/default-all-in-one.yaml). Keep in mind that this file is merely a starting point and will have to be modified according to your setup.
Deployment via Helm Chart (see below) is the recommended option.

#### Helm Chart

The Helm chart for deploying es-rollover-controller can be found at [msvechla.gitlab.io/es-rollover-controller](https://msvechla.gitlab.io/es-rollover-controller/).

All configuration options and installation instructions can be found on the Helm charts single page application.

#### Configuration Options

es-rollover-controller has the following configuration options:

```sh
GLOBAL OPTIONS:
   --kubeconfig value                     Path to a kubeconfig. Only required if out-of-cluster. [$KUBECONFIG]
   --master value                         The address of the Kubernetes API server. Overrides any value in kubeconfig. Only required if out-of-cluster. [$MASTER]
   --cache_ttl value                      TTL of cached annotations values that have already been discovered. (default: "12h") [$CACHE_TTL]
   --log_formatter value                  The log formatter. Should be one of [json, text]. (default: "text") [$LOG_FORMATTER]
   --namespace_scope value                The namespace which the controller should watch. If empty, the controller watches all namespaces. [$NAMESPACE_SCOPE]
   --workers value                        Number of workers that will process kubernetes events. (default: 2) [$WORKERS]
   --controller_id value                  ID of this es-rollover-controller instance. Adapt this if you have multiple controllers connected to the same elasticsearch cluster. (default: "es-rollover-controller") [$CONTROLLER_ID]
   --garbage_collection_interval value    Interval at which the garbage collection process should run to cleanup stale rollover artefacts, which are no longer managed by the controller. Requires elasticsearch v7.8.0+. A duration of e.g. 0m disables this feature. (default: "15m") [$GARBAGE_COLLECTION_INTERVAL]
   --elasticsearch_host value             The elasticsearch host. (default: "http://elastic") [$ELASTICSEARCH_HOST]
   --elasticsearch_port value             The elasticsearch port. (default: "9200") [$ELASTICSEARCH_PORT]
   --elasticsearch_username value         The elasticsearch username. [$ELASTICSEARCH_USERNAME]
   --elasticsearch_password value         The elasticsearch password. [$ELASTICSEARCH_PASSWORD]
   --elasticsearch_ca_cert_file value     Path to the elasticsearch ca certificate file used to connect to the cluster. [$ELASTICSEARCH_CA_CERT_FILE]
   --elasticsearch_timeout value          The elasticsearch response header / dialer timeout. (default: "30s") [$ELASTICSEARCH_TIMEOUT]
   --rollover_index_label_key value       Key of the pod label that contains the name of the rollover index. Takes precedence over rollover_index_annotation_key. (default: "elastic-index") [$ROLLOVER_INDEX_LABEL_KEY]
   --rollover_index_annotation_key value  Key of the pod annotation that contains the name of the rollover index. Usage of rollover_index_label_key preferred for performance reasons, see README for more info. [$ROLLOVER_INDEX_ANNOTATION_KEY]
   --write_alias_format value             Format String of the write alias, %s will be substituted with the index name. (default: "%s_writealias") [$WRITE_ALIAS_FORMAT]
   --index_template_format value          Format String of the index template, %s will be substituted with the index name. (default: "%s_es-rollover-controller") [$INDEX_TEMPLATE_FORMAT]
   --cleanup_legacy_index_templates       Whether to remove outdated legacy index templates, required if this controller was deployed pre 7.8.0 (default: false) [$CLEANUP_LEGACY_INDEX_TEMPLATES]
   --ilm_policy_name value                Name of the ILM policy that should be used. (default: "es-rollover-controller") [$ILM_POLICY_NAME]
   --ilm_policy_file value                Path to the ilm policy file that should be used. [$ILM_POLICY_FILE]
   --ilm_policy_label_key value           Optional key of the pod label that contains the name of the ilm policy to attach. If no label is found the default ILM policy will be attached. (default: "elastic-ilm-policy") [$ILM_POLICY_LABEL_KEY]
   --ilm_format value                     Format String of the rollover index, %s will be substituted with the index name. Has to end with -000001. (default: "%s-000001") [$ILM_FORMAT]
   --ilm_policy_overwrite                 Specifies whether the lifecycle policy is overwritten at startup. (default: false) [$ILM_POLICY_OVERWRITE]
   --help, -h                             show help (default: false)
   --version, -v                          print the version (default: false)
```

#### Default ILM Policy

By default an ILM policy with the following configuration is deployed:

```json
{
    "policy": {
        "phases": {
            "hot": {
                "actions": {
                    "rollover": {
                        "max_age": "30d",
                        "max_size": "50gb"
                    }
                }
            }
        }
    }
}
```

For an example on how to overwrite the default configuration, take a look at the [Quickstart Guide](deploy/examples/quickstart/quickstart-guide.md).

#### Kibana Data View Management (EXPERIMENTAL)

Since version `v0.10.0` the controller supports automatically creating [Data Views](https://www.elastic.co/guide/en/kibana/master/data-views.html) in Kibana.

This feature is considered experimental for now and therefor only supports a limited configuration.
We are actively looking for feedback. If further configuration options are required, please [create an issue and let us know](https://gitlab.com/msvechla/es-rollover-controller/-/issues).

When the `create_data_views` flag is set to `true`, the controller will automatically ensure that a matching data view is created in Kibana.
For now only a data view that matches the rollover index pattern is created, using the default index pattern configuration.

In case the data view already exists and has not been created via the controller, a warning message is logged.

### Configuring Filebeat

After es-rollover-controller is deployed, Filebeat should be setup inside the cluster to forward all logfiles to the rollover indices managed by the controller. Filebeat has to use the same label as es-rollover-controller to determine the elasticsearch output index name (`elastic-index` by default). An example Filebeat configuration can be found in [deploy/examples/quickstart/filebeat.yml](deploy/examples/quickstart/filebeat.yml)

### Configuring Elasticsearch

> HINT: es-rollover-controller requires elasticsearch >= 6.6.0

By default Elasticsearch will automatically create indices when a documents destination index does not exist yet. This behavior can be dangerous when using es-rollover-controller, as we need to ensure the controller creates the index with the correct rollover pattern and the matching write alias. Therefor it is recommended to turn of automatic index creation for all write alias index patterns. This can be configured in the following way:

```sh
curl -XPUT "https://localhost:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d'
{
    "persistent": {
        "action.auto_create_index": "-*_writealias,+*"
    }
}
'
```

This will turn off automatic index creation for all `*_writealias` indices, and enable it for all other indices, which is the default behavior. Make sure the replace `_writealias` with your custom write alias suffix, if you overwrote the default of the controller.

## Resources Managed by the Controller

The following resources are dynamically created inside elasticsearch based on pod labels detected by the controller:

- ILM Policy: The controller can create a custom ILM policy during the startup process, which will be attached to indices managed by the controller
- Initial Rollover Index: The rollover index containing the `-000001` rollover pattern suffix
- Write Alias: Write alias pointing to the current active index
- Index Templates: Index templates assigning a specified ILM policy to the created indices

For a better understanding of the technical background, take a look at the [official blogpost from elastic](https://www.elastic.co/blog/managing-time-based-indices-efficiently), which is introducing the rollover pattern, as well as the [example scenario](#example-scenario) below.

### Example Scenario

The following example scenario illustrates which resources are managed by the es-rollover-controller. All resources in the elasticsearch cluster below are managed via the controller.
The naming of those resources is defined both by configuration options of the es-rollover-controller and the labels attached to the pods inside the Kubernetes cluster.

Filebeat then forwards the logs to the relevant write alias in Elasticsearch, which in the beginning points to the initial rollover-index (e.g. `customapp-000001`), that will eventually get rolled-over as defined in the `es-rollover-controller` ILM policy.

![Example Scenario](images/example_scenario.svg "Example Scenario")

### Garbage Collection

Starting with version v0.2.0 of the controller, garbage collection of the rollover artefacts is enabled, when working with an elasticsearch cluster v7.8.0+.
The controller will try to cleanup all elasticsearch artefacts, that are no longer managed by the controller (meaning the rollover labels which initially triggered the creation of these artefacts are no longer present in the Kubernetes cluster).

This garbage collection is controlled via the config option `GARBAGE_COLLECTION_INTERVAL`. The feature can be disabled by configuring a zero value, such as `0s`.

## Upgrading Instructions

### Upgrading to Elasticsearch v7.8.0+

Starting with Elasticsearch v7.8.0, the controller supports [index templates v2](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/index-templates.html) and tagging it's artefacts, so they can be cleaned up via the controllers [garbage collection feature](https://gitlab.com/msvechla/es-rollover-controller#garbage-collection).

It is recommended to run the controller once with the `CLEANUP_LEGACY_INDEX_TEMPLATES` option enabled, so all legacy index templates can be removed. Afterwards this option can be disabled again.

## Advanced Configuration

The following section describes some of the more advanced configuration options of the controller.

### Date-Math Support for Indices

In some scenarios it might be desired to use [date math](https://www.elastic.co/guide/en/elasticsearch/reference/7.x/date-math-index-names.html#date-math-index-names) within index names, to highlight the date when a rollover has been performed.

The `--ilm_format` flag supports these date math expressions. It is important that values for this flag do not have to be URL escaped, as this will be done by the controller.

Example: `--ilm_format "<logs-{now{yyyy.MM.dd.HH.mm.ss}}-%s-000001>"` and example pod with label `elastic-index: helloworld`

- This will result in the following initial index:

```sh
❯ curl localhost:9200/_cat/indices
yellow open logs-2021.01.10.19.18.27-helloworld-000001 FeGN8N43QzicOwzoRwIU8g 1 1 0 0 208b 208b
```

- After triggering a rollover, the new index will be created according to the date math pattern:

```sh
❯ curl -XPOST localhost:9200/helloworld_writealias/_rollover
{"acknowledged":true,"shards_acknowledged":true,"old_index":"logs-2021.01.10.19.18.27-helloworld-000001","new_index":"logs-2021.01.10.19.19.03-helloworld-000002","rolled_over":true,"dry_run":false,"conditions":{}}
```

- Resulting in the following indices:

```sh
❯ curl localhost:9200/_cat/indices
yellow open logs-2021.01.10.19.18.27-helloworld-000001 FeGN8N43QzicOwzoRwIU8g 1 1 0 0 208b 208b
yellow open logs-2021.01.10.19.19.03-helloworld-000002 MV85WvwUSAW0hz-b9FXDvw 1 1 0 0 208b 208b
```

### Specifying Rollover Index via Labels and Annotations

You can specify the rollover index to use on your pods via labels ,annotations or both.

For performance reasons, in clusters with a large number of pods, the usage of labels is preferred, as the controller can filter the observed pods more efficiently based on a configured label key.
In scenarios where you need to support index names with [more than 63 characters](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-label-names), annotations can be used as a source by specifying the `rollover_index_annotation_key` flag.

Both `rollover_index_label_key` and `rollover_index_annotation_key` can be used in parallel. In this case resources will be created for both values specified on a pod.
When both keys are the same and both are referenced on a pod, the label value will take precedence over the annotation.

### Using Custom Component Templates

You can specify multiple component templates that will get merged into the index template created by the controller in order.

You can use the flag `--component_templates_annotation_key` to specify a name of a pod annotation key, that will contain the component template reference (`elastic-component-tempaltes` by default).

By specifying a comma separated list of component template names as a pod annotation, the index template and indices will be created accordingly:

For example:

yaml
```
  template:
    metadata:
      name: kube-testlog
      labels:
        elastic-index: kube-testlog
      annotations:
        elastic-component-templates: "my-component-template,another-one"
```

## Production Use cases and Users

*If you are using **es-rollover-controller** in a production environment, let us know by creating a MR and adding yourself to the list. It's always awesome to see a project put to good use 🚀*

Use cases of the controller:

- deployed to several hundred Kubernetes nodes [@share-now](https://www.share-now.com/)

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/es-rollover-controller/tags) or take a look at the [CHANGELOG.md](./CHANGELOG.md)

## Authors

- **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/es-rollover-controller/graphs/master) who participated in this project.

## License

[Apache 2.0 License](./LICENSE.md)  
Copyright (c) [2019] [Marius Svechla]

## Acknowledgments

- This project was initially based on the [official kubernetes sample-controller](https://github.com/kubernetes/sample-controller)
