/*
Copyright 2017 The Kubernetes Authors.
Modifications Copyright 2019 Marius Svechla

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"github.com/elastic/go-elasticsearch/v8/esapi"
	"github.com/stretchr/testify/assert"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/esutil"
	"mariussvechla.com/es-rollover-controller/internal/pkg/instrumentation"
	"mariussvechla.com/es-rollover-controller/internal/pkg/kbutil"
)

func TestKibanaIndexPatternSetup(t *testing.T) {
	for _, dockerImage := range esVersions {
		f := newFixture(t)
		conf, cleanupFunc, err := f.createTestStack(dockerImage)
		defer cleanupFunc()

		assert.NoError(t, err)

		// configure the controller
		conf.FlagRolloverIndexLabelKey = "elastic-index"
		conf.FlagCreateDataViews = true

		labelValue := "my-awesome-index"

		// create a test pod with elastic-index key for both rollover label and annotation
		pod := newPod(
			"kibanatest",
			map[string]string{"elastic-index": labelValue},
			map[string]string{},
		)
		f.podLister = append(f.podLister, pod)

		actualESActions, actualKBActions := []string{}, []string{}
		for i := 0; i < 2; i++ {
			t.Logf("running control loop #%d", i)
			newESActions, newKBActions := f.runControlLoop(conf, "default/kibanatest")
			actualESActions = append(actualESActions, newESActions...)
			actualKBActions = append(actualKBActions, newKBActions...)
		}
		expectedESActions := []string{
			esutil.ActionCreateIndexTemplate,
			esutil.ActionCreateRolloverIndex,
			esutil.ActionCreateILMPolicy,
		}
		expectedKBActions := []string{
			kbutil.ActionCreateDataView,
		}
		assert.ElementsMatch(t, expectedESActions, actualESActions)
		assert.ElementsMatch(t, expectedKBActions, actualKBActions)

		es, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("estest"))
		assert.NoError(t, err)

		kb, err := kbutil.NewKBClient(conf, instrumentation.NewMetricsCollector("kbtest"))
		assert.NoError(t, err)

		// ensure kibana index pattern has actually been created
		exists, _, err := kb.DataViewExistsAndIsUpToDate(es.GetKibanaIndexPattern(labelValue))
		assert.NoError(t, err)
		assert.True(t, exists)

		// create another test pod with the same value, expecting no kibana action as the index pattern already exists
		anotherPod := newPod(
			"anotherkibanatest",
			map[string]string{"elastic-index": labelValue},
			map[string]string{},
		)
		f.podLister = append(f.podLister, anotherPod)

		_, actualKBActions = f.runControlLoop(conf, "default/anotherkibanatest")
		expectedKBActions = []string{}
		assert.ElementsMatch(t, expectedKBActions, actualKBActions)
	}
}

// TestComponentTemplates ensures component Templates are created as specified in the annotation
func TestComponentTemplates(t *testing.T) {
	for _, dockerImage := range esVersions {
		f := newFixture(t)
		conf, cleanupFunc, err := f.createTestStack(dockerImage)
		defer cleanupFunc()

		assert.NoError(t, err)

		// configure the controller
		conf.FlagRolloverIndexAnnotationKey = "elastic-index"
		conf.FlagComponentTemplatesAnnotationKey = "elastic-components"

		expectedComponentTemplates := []string{"test", "yay"}

		es, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("component"))
		assert.NoError(t, err)

		// create the component templates
		for _, componentTemplateName := range expectedComponentTemplates {
			resp, err := es.GetClient().Cluster.PutComponentTemplate(
				componentTemplateName,
				strings.NewReader(`{"template": {"settings": {"number_of_shards": 1}}}`),
			)

			assert.NoError(t, err)
			assert.False(t, resp.IsError())
		}

		// create a test pod with elastic-index key for both rollover label and annotation
		pod := newPod(
			"components",
			map[string]string{},
			map[string]string{
				"elastic-index":      "fromannotation",
				"elastic-components": strings.Join(expectedComponentTemplates, ","),
			},
		)
		f.podLister = append(f.podLister, pod)

		actualActions, _ := f.runControlLoop(conf, "default/components")
		expectedActions := []string{
			esutil.ActionCreateIndexTemplate,
			esutil.ActionCreateRolloverIndex,
			esutil.ActionCreateILMPolicy,
		}
		assert.ElementsMatch(t, expectedActions, actualActions)

		// ensure component templates have been created
		indexTemplateName := es.GetIndexTemplateName("fromannotation")
		resp, res, err := es.RequestIndexTemplate(indexTemplateName)
		assert.NoError(t, err)
		assert.False(t, res.IsError())

		actualComponentTemplates := es.ParseComponentTemplatesFromIndexTemplate(indexTemplateName, resp)
		if es.HasNewIndexTemplatesSupport() {
			assert.Equal(t, &expectedComponentTemplates, actualComponentTemplates)
		} else {
			var expected *[]string
			assert.Equal(t, expected, actualComponentTemplates)
		}

		// run control loop again, no actions expected
		actualActions, _ = f.runControlLoop(conf, "default/components")
		expectedActions = []string{}
		assert.ElementsMatch(t, expectedActions, actualActions)

		// change the component template
		expectedComponentTemplates = []string{"test", "changed"}
		changedPod := newPod(
			"changedComponents",
			map[string]string{},
			map[string]string{
				"elastic-index":      "fromannotation",
				"elastic-components": strings.Join(expectedComponentTemplates, ","),
			},
		)
		f.podLister = append(f.podLister, changedPod)

		// create the changed component templates
		for _, componentTemplateName := range expectedComponentTemplates {
			resp, err := es.GetClient().Cluster.PutComponentTemplate(
				componentTemplateName,
				strings.NewReader(`{"template": {"settings": {"number_of_shards": 1}}}`),
			)

			assert.NoError(t, err)
			assert.False(t, resp.IsError())
		}

		actualActions, _ = f.runControlLoop(conf, "default/changedComponents")
		if es.HasNewIndexTemplatesSupport() {
			// new index templates are supported and they changed
			// update of index template expected
			assert.ElementsMatch(t, []string{esutil.ActionCreateIndexTemplate}, actualActions)
		} else {
			// no new index template support
			// change should not trigger update
			assert.ElementsMatch(t, []string{}, actualActions)
		}

		// ensure component templates have been updated
		resp, res, err = es.RequestIndexTemplate(indexTemplateName)
		assert.NoError(t, err)
		assert.False(t, res.IsError())

		actualComponentTemplates = es.ParseComponentTemplatesFromIndexTemplate(indexTemplateName, resp)
		if es.HasNewIndexTemplatesSupport() {
			assert.Equal(t, &expectedComponentTemplates, actualComponentTemplates)
		} else {
			var expected *[]string
			assert.Equal(t, expected, actualComponentTemplates)
		}
	}
}

// TestLabelPrecedence ensures rollover index specified via label takes precendence over annotations
func TestLabelPrecedence(t *testing.T) {
	for _, dockerImage := range esVersions {
		f := newFixture(t)
		conf, cleanupFunc, err := f.createTestStack(dockerImage)
		defer cleanupFunc()

		assert.NoError(t, err)

		// configure the controller
		conf.FlagRolloverIndexLabelKey = "elastic-index"
		conf.FlagRolloverIndexAnnotationKey = "elastic-index"

		// create a test pod with elastic-index key for both rollover label and annotation
		pod := newPod(
			"precedence",
			map[string]string{"elastic-index": "fromlabel"},
			map[string]string{"elastic-index": "fromannotation"},
		)
		f.podLister = append(f.podLister, pod)

		actualActions, _ := f.runControlLoop(conf, "default/precedence")
		expectedActions := []string{
			esutil.ActionCreateIndexTemplate,
			esutil.ActionCreateRolloverIndex,
			esutil.ActionCreateILMPolicy,
		}
		assert.ElementsMatch(t, expectedActions, actualActions)

		es, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("test"))
		assert.NoError(t, err)

		// ensure rollover artefacts have been created for fromLabel, not from Annotation, as label should take precedence
		fromLabel := es.GetIndexTemplateName("fromlabel")
		_, res, err := es.RequestIndexTemplate(fromLabel)

		assert.NoError(t, err)
		assert.False(t, res.IsError())
	}
}

// TestRollover ensures the basic rollover setup works
func TestRollover(t *testing.T) {
	const rolloverValue = "labelrollover"
	tests := []struct {
		name            string
		annotationKey   string
		labelKey        string
		annotationValue map[string]string
		labelValue      map[string]string
	}{
		{
			name:            "rollover_via_label",
			annotationKey:   "elastic-index",
			labelKey:        "elastic-index",
			annotationValue: map[string]string{},
			labelValue:      map[string]string{"elastic-index": rolloverValue},
		},
		{
			name:            "rollover_via_annotation",
			annotationKey:   "elastic-index",
			labelKey:        "elastic-index",
			annotationValue: map[string]string{"elastic-index": rolloverValue},
			labelValue:      map[string]string{},
		},
	}

	for _, dockerImage := range esVersions {
		for _, tt := range tests {
			t.Run(fmt.Sprintf("%s_%s", tt.name, dockerImage), func(t *testing.T) {
				f := newFixture(t)
				conf, cleanupFunc, err := f.createTestStack(dockerImage)
				defer cleanupFunc()

				assert.NoError(t, err)

				// configure the controller
				conf.FlagRolloverIndexAnnotationKey = tt.annotationKey
				conf.FlagRolloverIndexLabelKey = tt.labelKey

				// create a test pod
				pod := newPod("rollover", tt.labelValue, tt.annotationValue)
				f.podLister = append(f.podLister, pod)

				actualActions, _ := f.runControlLoop(conf, "default/rollover")
				expectedActions := []string{
					esutil.ActionCreateIndexTemplate,
					esutil.ActionCreateRolloverIndex,
					esutil.ActionCreateILMPolicy,
				}
				assert.ElementsMatch(t, expectedActions, actualActions)

				es, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("test"))
				assert.NoError(t, err)

				// ensure rollover artefacts have been created
				fromLabel := es.GetIndexTemplateName(rolloverValue)
				_, res, err := es.RequestIndexTemplate(fromLabel)

				assert.NoError(t, err)
				assert.False(t, res.IsError())

				// check whether the writealias exists
				writeAliasExistsReq := esapi.IndicesExistsRequest{
					Index: []string{es.GetWriteAlias(rolloverValue)},
				}
				resp, err := writeAliasExistsReq.Do(context.Background(), es.GetClient().Transport)

				assert.NoError(t, err, "retrieving writealias")
				assert.False(t, resp.IsError(), "retrieving writealias")

				// check whether the initial rollover index exists
				initialRolloverExistsReq := esapi.IndicesExistsRequest{
					Index: []string{es.GetRolloverIndex(rolloverValue)},
				}
				resp, err = initialRolloverExistsReq.Do(context.Background(), es.GetClient().Transport)

				assert.NoError(t, err, "retrieving initial rollover index")
				assert.False(t, resp.IsError(), "retrieving initial rollover index")

				// trigger rollover
				rolloverReq := esapi.IndicesRolloverRequest{
					Alias: es.GetWriteAlias(rolloverValue),
				}
				resp, err = rolloverReq.Do(context.Background(), es.GetClient().Transport)

				assert.NoError(t, err, "rolling over initial index")
				assert.False(t, resp.IsError(), "rolling ober initial index")

				// check whether the rolled-over index exists
				secondRolloverExistsReq := esapi.IndicesExistsRequest{
					Index: []string{
						strings.Replace(es.GetRolloverIndex(rolloverValue), config.ILMIncrementPattern, "-000002", 1),
					},
				}
				resp, err = secondRolloverExistsReq.Do(context.Background(), es.GetClient().Transport)

				assert.NoError(t, err, "retrieving rolled-over index")
				assert.False(t, resp.IsError(), "retrieving rolled-over index", resp.String())
			})
		}
	}
}

// TestTestEndToEnd runs a real-life comparable end to end scenario which tests most of the controllers behavior
func TestEndToEnd(t *testing.T) {
	for _, dockerImage := range esVersions {
		t.Run(fmt.Sprintf("es_v_%s", dockerImage), func(t *testing.T) {
			f := newFixture(t)
			t.Log("Creating Test ES Instance...")
			conf, cleanupFunc, err := f.createTestStack(dockerImage)
			defer cleanupFunc()
			assert.NoError(t, err)

			t.Log("testing es version")
			cl, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("test"))
			assert.NoError(t, err)

			expectedVersion := dockerImage
			assert.Equal(t, expectedVersion, cl.Version.Original())

			assert.Nil(t, err)
			t.Log("Running end to end tests...")
			pod := newPod("test", map[string]string{"elastic-index": "test"}, map[string]string{})
			f.podLister = append(f.podLister, pod)

			conf.FlagRolloverIndexLabelKey = "elastic-index"
			conf.FlagRolloverIndexAnnotationKey = "elastic-index-annotation"

			actualActions, _ := f.runControlLoop(conf, "default/test")
			expectedActions := []string{
				esutil.ActionCreateIndexTemplate,
				esutil.ActionCreateRolloverIndex,
				esutil.ActionCreateILMPolicy,
			}
			assert.ElementsMatch(t, expectedActions, actualActions)

			// add another pod with the same label, no actions expected
			secondPod := newPod("secondtest", map[string]string{"elastic-index": "test"}, map[string]string{})
			f.podLister = append(f.podLister, secondPod)
			actualActions, _ = f.runControlLoop(conf, "default/secondtest")
			expectedActions = []string{}
			assert.ElementsMatch(
				t,
				expectedActions,
				actualActions,
				"got the following actual actions: %s",
				actualActions,
			)

			// add a third pod with a new key, usual actions expected
			helloWorldPod := newPod("thirdtest", map[string]string{"elastic-index": "helloworld"}, map[string]string{})
			f.podLister = append(f.podLister, helloWorldPod)
			actualActions, _ = f.runControlLoop(conf, "default/thirdtest")
			expectedActions = []string{esutil.ActionCreateIndexTemplate, esutil.ActionCreateRolloverIndex}
			assert.ElementsMatch(t, expectedActions, actualActions)

			// add a fourth pod with a new index and ilm policy, usual actions expected
			fourthPod := newPod(
				"fourthtest",
				map[string]string{"elastic-index": "custom-ilm", conf.FlagILMPolicyLabelKey: "custom-ilm"},
				map[string]string{},
			)
			f.podLister = append(f.podLister, fourthPod)
			actualActions, _ = f.runControlLoop(conf, "default/fourthtest")
			expectedActions = []string{esutil.ActionCreateIndexTemplate, esutil.ActionCreateRolloverIndex}
			assert.ElementsMatch(t, expectedActions, actualActions)

			// add another pod with a new index and ilm policy via annotations, usual actions expected
			annotationsPod := newPod(
				"annotationtest",
				map[string]string{},
				map[string]string{conf.FlagRolloverIndexAnnotationKey: "annotationtest"},
			)
			f.podLister = append(f.podLister, annotationsPod)
			actualActions, _ = f.runControlLoop(conf, "default/annotationtest")
			expectedActions = []string{esutil.ActionCreateIndexTemplate, esutil.ActionCreateRolloverIndex}
			assert.ElementsMatch(t, expectedActions, actualActions)

			// add pod with both label and annotation configured, actions for both expected
			bothPod := newPod(
				"bothtest",
				map[string]string{"elastic-index": "bothtestlabel"},
				map[string]string{conf.FlagRolloverIndexAnnotationKey: "bothtestannotation"},
			)
			f.podLister = append(f.podLister, bothPod)
			actualActions, _ = f.runControlLoop(conf, "default/bothtest")
			expectedActions = []string{
				esutil.ActionCreateIndexTemplate,
				esutil.ActionCreateRolloverIndex,
				esutil.ActionCreateIndexTemplate,
				esutil.ActionCreateRolloverIndex,
			}
			assert.ElementsMatch(t, expectedActions, actualActions)

			es, err := esutil.NewESClient(conf, instrumentation.NewMetricsCollector("test"))
			assert.NoError(t, err)

			// ensure elastic-index: test has correct index template set
			helloWorld := es.GetIndexTemplateName("helloworld")
			helloWorldBody, res, err := es.RequestIndexTemplate(helloWorld)

			assert.NoError(t, err)
			assert.False(t, res.IsError())

			actualHelloWorldILM := es.ParseILMPolicyNameFromIndexTemplate(helloWorld, helloWorldBody)
			assert.Equal(t, conf.FlagILMPolicyName, actualHelloWorldILM)

			// ensure elastic-index: custom-ilm has correct ilm custom-ilm set in index template
			customILM := es.GetIndexTemplateName("custom-ilm")
			customILMBody, res, err := es.RequestIndexTemplate(customILM)

			assert.NoError(t, err)
			assert.False(t, res.IsError())
			actualCustomILMILM := es.ParseILMPolicyNameFromIndexTemplate(customILM, customILMBody)
			assert.Equal(t, "custom-ilm", actualCustomILMILM)

			// adapt helloWorldPod to also use a custom ILM and see if the index template was adjusted
			helloWorldPod.SetLabels(
				map[string]string{"elastic-index": "helloworld", conf.FlagILMPolicyLabelKey: "hello-ilm"},
			)
			actualActions, _ = f.runControlLoop(conf, "default/thirdtest")
			expectedActions = []string{esutil.ActionCreateIndexTemplate}
			assert.ElementsMatch(t, expectedActions, actualActions)

			helloWorldBody, res, err = es.RequestIndexTemplate(helloWorld)

			assert.NoError(t, err)
			assert.False(t, res.IsError())

			actualHelloWorldILM = es.ParseILMPolicyNameFromIndexTemplate(helloWorld, helloWorldBody)
			assert.Equal(t, "hello-ilm", actualHelloWorldILM)

			if es.HasNewIndexTemplatesSupport() {
				t.Run("garbage collection", func(t *testing.T) {
					_, informer := f.newController(conf)
					removed, err := cleanupStaleRolloverIndexArtefacts(informer.Core().V1().Pods(), es, conf)
					assert.NoError(t, err)
					assert.Empty(t, removed)

					// remove the fourth pod with elastic index custom-ilm, garbage collection should kick in
					f.podLister = deletePodByName("fourthtest", f.podLister)
					_, informer = f.newController(conf)
					removed, err = cleanupStaleRolloverIndexArtefacts(informer.Core().V1().Pods(), es, conf)
					assert.NoError(t, err)
					assert.Equal(t, []string{"custom-ilm_es-rollover-controller"}, removed)

					// remove the next two pods with elstic index helloworld and test, garbage collection should kick in only for helloworld, as there is another pod with the label test
					f.podLister = deletePodByName("secondtest", f.podLister)
					f.podLister = deletePodByName("thirdtest", f.podLister)
					_, informer = f.newController(conf)
					removed, err = cleanupStaleRolloverIndexArtefacts(informer.Core().V1().Pods(), es, conf)
					assert.NoError(t, err)
					assert.Equal(t, []string{"helloworld_es-rollover-controller"}, removed)

					// remove annotation pod, garbage collection should kick in
					f.podLister = deletePodByName("annotationtest", f.podLister)
					_, informer = f.newController(conf)
					removed, err = cleanupStaleRolloverIndexArtefacts(informer.Core().V1().Pods(), es, conf)
					assert.NoError(t, err)
					assert.Equal(t, []string{"annotationtest_es-rollover-controller"}, removed)
				})
			}
		})
	}
}
