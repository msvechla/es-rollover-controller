/*
Copyright 2017 The Kubernetes Authors.
Modifications Copyright 2019 Marius Svechla

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"fmt"
	"time"

	"github.com/jellydator/ttlcache/v2"
	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	kubeinformers "k8s.io/client-go/informers"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	typedcorev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/esutil"
	"mariussvechla.com/es-rollover-controller/internal/pkg/instrumentation"
	"mariussvechla.com/es-rollover-controller/internal/pkg/kbutil"
	"mariussvechla.com/es-rollover-controller/internal/pkg/kubeutil"
)

const (
	// SuccessSynced is used as part of the Event 'reason' when a Pod is synced
	SuccessSynced = "Synced"
	// MessageResourceSynced is the message used for an Event fired when a Pod
	// is synced successfully
	MessageResourceSynced = "Pod synced successfully"
)

// Controller is the controller implementation for Pod resources
type Controller struct {
	// kubeclientset is a standard kubernetes clientset
	kubeclientset kubernetes.Interface

	podsLister corelisters.PodLister
	podsSynced cache.InformerSynced

	// workqueue is a rate limited work queue. This is used to queue work to be
	// processed instead of performing it as soon as a change happens. This
	// means we can ensure we only process a fixed amount of resources at a
	// time, and makes it easy to ensure we are never processing the same item
	// simultaneously in two different workers.
	workqueue workqueue.RateLimitingInterface
	// recorder is an event recorder for recording Event resources to the
	// Kubernetes API.
	recorder record.EventRecorder

	indexNameCache   *ttlcache.Cache
	metricsCollector *instrumentation.MetricsCollector
	esClient         *esutil.ESClient
	kbClient         *kbutil.KBClient
	config           *config.Config
}

// NewController returns a new sample controller
func NewController(
	kubeclientset kubernetes.Interface,
	podInformer coreinformers.PodInformer,
	config *config.Config,
	stopCh <-chan struct{},
) *Controller {
	// create event broadcaster
	log.Info("Creating event broadcaster")
	eventBroadcaster := record.NewBroadcaster()
	eventBroadcaster.StartLogging(log.Infof)
	eventBroadcaster.StartRecordingToSink(&typedcorev1.EventSinkImpl{Interface: kubeclientset.CoreV1().Events("")})
	recorder := eventBroadcaster.NewRecorder(scheme.Scheme, corev1.EventSource{Component: config.FlagControllerID})

	metricsCollector := instrumentation.NewMetricsCollector(config.FlagControllerID)

	// create elasticsearch client
	esClient, err := esutil.NewESClient(config, metricsCollector)
	if err != nil {
		log.Fatalf("Unable to connect to elasticsearch: %s", err)
	}
	log.Info("Successfully connected to elasticsearch")

	log.Info("Bootstrapping elasticsearch setup")

	err = esClient.Bootstrap()
	if err != nil {
		log.Fatalf("Error during elasticsearch bootstrapping: %s", err)
	}

	if esClient.HasNewIndexTemplatesSupport() {
		log.Info("New index template support detected")
		err = esClient.CleanupLegacyIndexTemplates()
		if err != nil {
			log.Errorf("Error cleaning up legacy index tempates: %s", err)
		}
	}
	log.Info("Elasticsearch bootstrapping completed")

	// create kibana client
	var kbClient *kbutil.KBClient
	if config.FlagCreateDataViews {
		kbClient, err = kbutil.NewKBClient(config, metricsCollector)
		if err != nil {
			log.Fatalf("Unable to connect to kibana: %s", err)
		}
		log.Info("Successfully connected to kibana")
	}

	// create the controller
	controller := &Controller{
		kubeclientset:    kubeclientset,
		podsLister:       podInformer.Lister(),
		podsSynced:       podInformer.Informer().HasSynced,
		workqueue:        workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Pods"),
		recorder:         recorder,
		indexNameCache:   ttlcache.NewCache(),
		esClient:         esClient,
		kbClient:         kbClient,
		config:           config,
		metricsCollector: metricsCollector,
	}

	// setup metrics exporter
	if config.FlagExposePrometheusMetrics {
		go controller.metricsCollector.ServeMetrics()
	}

	// setup the cache
	ttl, err := time.ParseDuration(config.FlagCacheTTL)
	if err != nil {
		log.Fatalf("Error parsing duration from Cache TTL: %s", ttl)
	}

	controller.indexNameCache.SetTTL(ttl)
	controller.indexNameCache.SkipTTLExtensionOnHit(true)
	log.Infof("Successfully created Cache with TTL %s", ttl)

	// set up an event handler for when Pods resources change
	log.Info("Setting up event handlers")

	podInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: controller.enqueuePod,
		UpdateFunc: func(old, new interface{}) {
			controller.enqueuePod(new)
		},
	})

	// setup garbage collection
	gcInterval, err := time.ParseDuration(config.FlagGarbageCollectionInterval)
	if err != nil {
		log.Fatalf("Error parsing duration from GarbageCollectionInterval: %s", ttl)
	}

	if esClient.HasNewIndexTemplatesSupport() && gcInterval.Seconds() > 0 {
		log.Infof("Starting rollover index garbage collector with interval %s", gcInterval.String())
		startGarbageCollector(podInformer, esClient, config, stopCh, gcInterval)
	}

	return controller
}

// Run will set up the event handlers for types we are interested in, as well
// as syncing informer caches and starting workers. It will block until stopCh
// is closed, at which point it will shutdown the workqueue and wait for
// workers to finish processing their current work items.
func (c *Controller) Run(threadiness int, stopCh <-chan struct{}) error {
	defer utilruntime.HandleCrash()
	defer c.workqueue.ShutDown()

	// Start the informer factories to begin populating the informer caches
	log.Info("Starting Controller")

	// Wait for the caches to be synced before starting workers
	log.Info("Waiting for informer caches to sync")
	if ok := cache.WaitForCacheSync(stopCh, c.podsSynced); !ok {
		return fmt.Errorf("failed to wait for caches to sync")
	}

	log.Info("Starting workers")

	// Launch two workers to process pod resources
	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	log.Info("Started workers")
	<-stopCh
	log.Info("Shutting down workers")

	return nil
}

// runWorker is a long-running function that will continually call the
// processNextWorkItem function in order to read and process a message on the
// workqueue.
func (c *Controller) runWorker() {
	for c.processNextWorkItem() {
	}
}

// processNextWorkItem will read a single work item off the workqueue and
// attempt to process it, by calling the syncHandler.
func (c *Controller) processNextWorkItem() bool {
	obj, shutdown := c.workqueue.Get()

	if shutdown {
		return false
	}

	// We wrap this block in a func so we can defer c.workqueue.Done.
	err := func(obj interface{}) error {
		// We call Done here so the workqueue knows we have finished
		// processing this item. We also must remember to call Forget if we
		// do not want this work item being re-queued. For example, we do
		// not call Forget if a transient error occurs, instead the item is
		// put back on the workqueue and attempted again after a back-off
		// period.
		defer c.workqueue.Done(obj)
		var key string
		var ok bool
		// We expect strings to come off the workqueue. These are of the
		// form namespace/name. We do this as the delayed nature of the
		// workqueue means the items in the informer cache may actually be
		// more up to date that when the item was initially put onto the
		// workqueue.
		if key, ok = obj.(string); !ok {
			// As the item in the workqueue is actually invalid, we call
			// Forget here else we'd go into a loop of attempting to
			// process a work item that is invalid.
			c.workqueue.Forget(obj)
			utilruntime.HandleError(fmt.Errorf("expected string in workqueue but got %#v", obj))
			return nil
		}
		// Run the syncHandler, passing it the namespace/name string of the
		// Pod resource to be synced.
		if err := c.syncHandler(key); err != nil {
			// Put the item back on the workqueue to handle any transient errors.
			c.workqueue.AddRateLimited(key)
			return fmt.Errorf("error syncing '%s': %s, requeuing", key, err.Error())
		}
		// Finally, if no error occurs we Forget this item so it does not
		// get queued again until another change happens.
		c.workqueue.Forget(obj)
		log.Infof("Successfully synced '%s'", key)
		return nil
	}(obj)
	if err != nil {
		utilruntime.HandleError(err)
		return true
	}

	return true
}

// syncHandler compares the actual state with the desired, and attempts to
// converge the two
func (c *Controller) syncHandler(key string) error {
	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		utilruntime.HandleError(fmt.Errorf("invalid resource key: %s", key))
		return nil
	}

	// Get the Pod resource with this namespace/name
	pod, err := c.podsLister.Pods(namespace).Get(name)
	if err != nil {
		// The Pod resource may no longer exist, in which case we stop
		// processing.
		if errors.IsNotFound(err) {
			utilruntime.HandleError(fmt.Errorf("pod '%s' in work queue no longer exists", key))
			return nil
		}

		return err
	}

	// check if a custom ILM policy label was specified
	customILMPolicy := pod.GetLabels()[c.config.FlagILMPolicyLabelKey]

	// check if component template values have been specified
	componentTemplatesValue := pod.GetAnnotations()[c.config.FlagComponentTemplatesAnnotationKey]

	// get the rollover key and values from the pods labels and annotations
	keyValuePairs := kubeutil.GetRolloverIndexKVForPod(pod, c.config)

	for _, rolloverValue := range keyValuePairs {
		ttlCacheKey := getTTLCacheKey(rolloverValue, customILMPolicy)

		_, lookupErr := c.indexNameCache.Get(ttlCacheKey)

		c.metricsCollector.ObserveCacheMetics(c.indexNameCache)

		if lookupErr == ttlcache.ErrNotFound {
			// the controller discovers this index name or ilm policy for the first time, we might have to reconcile
			log.Infof("Index %s and ilm policy not found in cache, ensuring rollover setup...", rolloverValue)

			// set up the index template
			templateUpToDate, err := c.esClient.IndexTemplateUpToDate(
				rolloverValue,
				customILMPolicy,
				componentTemplatesValue,
			)
			if err != nil {
				log.Errorf("Error checking current template: %s", err)
				return err
			}

			if !templateUpToDate {
				err = c.esClient.EnsureIndexTemplate(rolloverValue, customILMPolicy, componentTemplatesValue)
				if err != nil {
					return err
				}
				log.Infof("Successfully updated index template %s", c.esClient.GetIndexTemplateName(rolloverValue))
			}

			// set up the rollover alias
			aliasExists, err := c.esClient.AliasExists(rolloverValue)
			if err != nil {
				log.Errorf("Error checking alias existence: %s", err)
				return err
			}

			if !aliasExists {
				err := c.esClient.CreateRolloverIndex(rolloverValue, customILMPolicy)
				if err != nil {
					log.Errorf("Error creating rollover index and alias: %s", err)
					return err
				}

				log.Infof(
					"Successfully created rollover index %s with alias %s",
					c.esClient.GetRolloverIndex(rolloverValue),
					c.esClient.GetWriteAlias(rolloverValue),
				)

				// update cache as creation was successful
				c.indexNameCache.Set(rolloverValue, rolloverValue)

				c.recorder.Event(pod, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
				continue
			}

			// setup data view in Kibana
			if c.config.FlagCreateDataViews {
				err := c.kbClient.EnsureDataViewExists(c.esClient.GetKibanaIndexPattern(rolloverValue))
				if err != nil {
					log.Errorf("Error creating kibana data view: %s", err)
					return err
				}
				log.Infof(
					"Successfully ensured kibana data view %s exists",
					c.esClient.GetKibanaIndexPattern(rolloverValue),
				)
			}

			// remember that we have already setup this index successfully
			c.indexNameCache.Set(getTTLCacheKey(rolloverValue, customILMPolicy), rolloverValue)
		} else {
			log.Infof("Cache lookup for index %s successful, skipping rollover setup", rolloverValue)
		}
	}

	c.recorder.Event(pod, corev1.EventTypeNormal, SuccessSynced, MessageResourceSynced)
	return nil
}

// enqueuePod takes a Pod resource and converts it into a namespace/name
// string which is then put onto the work queue. This method should *not* be
// passed resources of any type other than Pod.
func (c *Controller) enqueuePod(obj interface{}) {
	var key string
	var err error
	if key, err = cache.MetaNamespaceKeyFunc(obj); err != nil {
		utilruntime.HandleError(err)
		return
	}
	c.workqueue.Add(key)
}

// createRolloverIndexPodInformerFactory returns a pod informer factory for a specific label selector
func createRolloverIndexPodInformerFactory(
	kubeClient kubernetes.Interface,
	labelSelector string,
	annotationSelector string,
	namespaceScope string,
) kubeinformers.SharedInformerFactory {
	// if an annotationSelector has been specified, we can not filter the pods by the label only, we have to consider all pods
	// as we can not filter by annotation
	if annotationSelector != "" {
		labelSelector = ""
	}

	podInformerFactory := kubeinformers.NewSharedInformerFactoryWithOptions(
		kubeClient,
		time.Second*60,
		kubeinformers.WithNamespace(namespaceScope),
		kubeinformers.WithTweakListOptions(
			func(opt *metav1.ListOptions) {
				opt.LabelSelector = labelSelector
			},
		),
	)
	return podInformerFactory
}

// getTTLCacheKey returns the string to use as key for the TTL cache
func getTTLCacheKey(indexName string, customILMPolicy string) string {
	return fmt.Sprintf("%s-%s", indexName, customILMPolicy)
}
